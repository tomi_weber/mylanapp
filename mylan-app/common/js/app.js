
var accordion;

accordion = function () {

    var activeClass,
        parentElement,
        element,
        container,
        init,
        runAccordion;

    activeClass = 'is-active';

    runAccordion = function () {
            var $this = $(this);
            if ($this.parent(parentElement).hasClass(activeClass)) {
                $this.parents(container).find(parentElement).removeClass(activeClass);
                $this.parent(parentElement).removeClass(activeClass);
            } else {
                $this.parents(container).find(parentElement).removeClass(activeClass);
                $this.parent(parentElement).addClass(activeClass);
            }
        },
        init = function (options) {
            parentElement = options.parentElement;
            element = options.element;
            container = options.container;
            $(element).on('click', runAccordion);
        };

    return {
        init: init
    };

}

$(function () {
    var acc1;
    acc1 = new accordion();
    acc1.init({
        'container': '#js-accordion',
        'parentElement': '.js-accordion_item',
        'element': '.js-accordion_title'
    });
    $('#js-login').validate();
    
});