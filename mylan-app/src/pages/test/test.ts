import { Component, ViewChild } from '@angular/core';
import { NavController, App, ViewController, Tabs } from 'ionic-angular'
import { AuthService } from '../../providers/auth-service/auth-service';
import { Login } from '../login/login';
import { AgendaPage } from '../agenda/agenda';
import { AlertController, MenuController  } from 'ionic-angular';
import * as $ from 'jquery';
import { Events } from 'ionic-angular';

@Component({
  selector: 'page-test',
  templateUrl: 'test.html'
})
export class TestPage {

  userPostData = {"visitor_id":""};
  responseData: any;
    ankiety: any;
    start: Boolean = false;
    startTime: number = 0;
    votes: any[] = [];
    txt: any;
    txt2: any;
    ranking: any[] = [];
    tab: any[] = [];
    rankingUser: any;

  constructor(public navCtrl: NavController,
              public viewCtrl: ViewController,
              public authService:AuthService,
              public app: App,
              private alertCtrl: AlertController, public events: Events) {
      this.events.publish('hideHeader', { isHidden: false});
      const data = JSON.parse(localStorage.getItem('userData'));
      this.userPostData.visitor_id = data.visitor_id;

      this.tab.push({active : false});
      this.tab.push({active : false});
      this.tab.push({active : false});

      this.authService.postData('ankietaList', this.userPostData,'polltest/get').then((result) => {
          this.responseData = result;

          if(this.responseData.status == 'ok') {
              this.ankiety = JSON.parse(this.responseData.polls);
              this.txt = JSON.parse(this.responseData.txt);
              this.txt2 = JSON.parse(this.responseData.txt2);


              this.authService.postData('rankingListUser', this.userPostData,'polltest/rank/' + this.userPostData.visitor_id).then((result) => {
                  this.responseData = result;

                  //if(this.responseData.status == 'ok') {
                      this.rankingUser = JSON.parse(this.responseData.aswer);

                      this.authService.postData('rankingList', this.userPostData,'polltest/rank').then((result) => {
                          this.responseData = result;

                          if(this.responseData.status == 'ok') {
                              this.ranking = JSON.parse(this.responseData.answer);
                              console.log(this.ranking);
                                let ii = 1;
                              for(const item in this.ranking) {
                                  if(this.ranking[item].user_id == parseInt(this.userPostData.visitor_id)) {
                                      this.rankingUser.miejsce = ii;
                                  }
                                  ii++;
                              }

                              if(!this.ankiety || this.ankiety == 'false' || this.ankiety == false) {
                                  this.tab[2].active = true;
                              }

                          }
                      }, (err) => {
                          console.log('Error' + err);
                      });

                  //}
              }, (err) => {
                  console.log('Error' + err);
              });



              for(const poll in this.ankiety) {
                  this.votes.push(0);
              }

              console.log(this.ankiety);
              console.log(this.votes);

          }
      }, (err) => {
          console.log('Error' + err);
      });

  }

    showAgenda(date) {
        var parentElement = '.js-accordion_item';
        var activeClass = 'is-active';
        var container = '#js-accordion';

        var $this = $('#js-accordion_title2_' + date);

        setTimeout(() => {
            if ($this.parent(parentElement).hasClass(activeClass)) {
                $this.parents(container).find(parentElement).removeClass(activeClass);
                $this.parent(parentElement).removeClass(activeClass);
            } else {
                $this.parents(container).find(parentElement).removeClass(activeClass);
                $this.parent(parentElement).addClass(activeClass);
            }
        }, 300);
    }

    startVote() {
        this.start = true;
        this.startTime = Date.now();
        console.log(this.startTime);
    }

    vote() {
        console.log(Date.now());
        var time = Date.now()-this.startTime;
        this.authService.postData('ankietaVote', this.votes,'polltest/vote/' + this.userPostData.visitor_id + '/' + time).then((result) => {
            this.responseData = result;
            if(this.responseData.status == 'ok') {
                this.authService.postData('rankingListUser', this.userPostData,'polltest/rank/' + this.userPostData.visitor_id).then((result) => {
                    this.responseData = result;

                    if(this.responseData.status == 'ok') {
                        this.rankingUser = JSON.parse(this.responseData.aswer);
                        this.authService.postData('rankingList', this.userPostData,'polltest/rank').then((result) => {
                            this.responseData = result;

                            if(this.responseData.status == 'ok') {
                                this.ranking = JSON.parse(this.responseData.answer);
                                let ii = 1;
                                for(const item in this.ranking) {
                                    if(this.ranking[item].user_id == parseInt(this.userPostData.visitor_id)) {
                                        this.rankingUser.miejsce = ii;
                                    }
                                    ii++;
                                }
                                this.tab[2].active = true;
                                this.tab[0].active = false;
                                this.tab[1].active = false;
                                this.presentAlert();
                                this.ankiety = false;
                            }
                        }, (err) => {
                            console.log('Error' + err);
                        });

                    }
                }, (err) => {
                    console.log('Error' + err);
                });

            } else {
                alert('Wystąpił błąd podczas oddawania głosu.');
            }
        }, (err) => {
            console.log('Error' + err);
        });
    }

    toggleSection(i) {
        if(!this.tab[i].active) {
            this.tab[0].active = false;
            this.tab[1].active = false;
            this.tab[2].active = false;
        }

        this.tab[i].active = !this.tab[i].active;
    }

    presentAlert() {
        let alert = this.alertCtrl.create({
            title: 'Dziękujemy za oddanie głosu',
            subTitle: 'Liczba Twoich poprawnych odpowiedzi: ' + this.rankingUser.odpowiedzi +
            '<br>' +
            'Twój czas: ' + this.rankingUser.czas +
            '<br>' +
            'Aktualnie zajmujesz ' + this.rankingUser.miejsce + ' miejsce',
            buttons: ['Zamknij']
        });
        alert.present();
    }


}
