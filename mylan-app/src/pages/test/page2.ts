import { Component, ViewChild } from '@angular/core';
import { NavController, App, ViewController} from 'ionic-angular'
import { AuthService } from '../../providers/auth-service/auth-service';
import { Login } from '../login/login';
import { AgendaPage } from '../agenda/agenda';
import { AlertController, MenuController  } from 'ionic-angular';
import { TestPage } from './test';
import * as $ from 'jquery';

@Component({
    selector: 'page-test3',
    templateUrl: 'page2.html'
})
export class Page2Page {

    userPostData = {"visitor_id":"","answer_id":[]};
    responseData: any;
    ankiety: any;
    start: Boolean = false;
    startTime: number = 0;
    votes: any[] = [];
    txt: any;

    constructor(public navCtrl: NavController,
                public viewCtrl: ViewController,
                public authService:AuthService,
                public app: App,
                private alertCtrl: AlertController,
                menu: MenuController) {

        menu.enable(true);

        const data = JSON.parse(localStorage.getItem('userData'));
        this.userPostData.visitor_id = data.visitor_id;

        this.authService.postData('ankietaList', this.userPostData,'polltest/get').then((result) => {
            this.responseData = result;

            if(this.responseData.status == 'ok') {
                this.ankiety = JSON.parse(this.responseData.polls);
                this.txt = JSON.parse(this.responseData.txt);

                var ii = 0;
                for(const poll in this.ankiety) {
                    this.votes.push(0);
                }

                console.log(this.ankiety);
                console.log(this.votes);

            }
        }, (err) => {
            console.log('Error' + err);
        });

    }


    startVote() {
        this.start = true;
        this.startTime = Date.now();
        console.log(this.startTime);
    }

    vote() {
        console.log(Date.now());
        var time = Date.now()-this.startTime;
        this.authService.postData('ankietaVote', this.votes,'polltest/vote/' + this.userPostData.visitor_id + '/' + time).then((result) => {
            this.responseData = result;
            if(this.responseData.status == 'ok') {
                this.presentAlert();
                this.navCtrl.parent.select(2);
                this.ankiety = false;
            } else {
                alert('Wystąpił błąd podczas oddawania głosu.');
            }
        }, (err) => {
            console.log('Error' + err);
        });
    }

    presentAlert() {
        let alert = this.alertCtrl.create({
            title: 'Dziękujemy za oddanie głosu',
            subTitle: '',
            buttons: ['Zamknij']
        });
        alert.present();
    }



}
