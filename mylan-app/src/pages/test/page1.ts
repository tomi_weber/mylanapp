import { Component } from '@angular/core';
import { NavController, App, ViewController } from 'ionic-angular'
import { AuthService } from '../../providers/auth-service/auth-service';

@Component({
    selector: 'page-test2',
    templateUrl: 'page1.html'
})
export class Page1Page {

    userPostData = {"visitor_id":"","answer_id":[]};
    responseData: any;
    ankiety: any[] = [];

    constructor(public navCtrl: NavController,
                public viewCtrl: ViewController,
                public authService:AuthService,
                public app: App) {
    }
}
