import { Component, ViewChild } from '@angular/core';
import { NavController, App, ViewController, Tabs } from 'ionic-angular'
import { AuthService } from '../../providers/auth-service/auth-service';
import { Login } from '../login/login';
import { AgendaPage } from '../agenda/agenda';
import { AlertController, MenuController  } from 'ionic-angular';
import { KtoPage } from '../kto/kto';
import * as $ from 'jquery';

@Component({
    selector: 'page-test4',
    templateUrl: 'page3.html'
})
export class Page3Page {

    userPostData = {"visitor_id":""};
    responseData: any;
    ranking: any[] = [];
    rankingUser: any;

    constructor(public navCtrl: NavController,
                public viewCtrl: ViewController,
                public authService:AuthService,
                public app: App,
                menu: MenuController) {

        menu.enable(true);

        const data = JSON.parse(localStorage.getItem('userData'));
        this.userPostData.visitor_id = data.visitor_id;

        /*this.authService.postData('rankingList', this.userPostData,'polltest/rank').then((result) => {
            this.responseData = result;

            if(this.responseData.status == 'ok') {
                this.ranking = JSON.parse(this.responseData);

                // for(var key in json)
                //   if(json.hasOwnProperty(key))
                //     this.ankiety.push([key, json[key]]); // each item is an array in format [key, value]

                console.log(this.ranking);

            }
        }, (err) => {
            console.log('Error' + err);
        });*/

        this.authService.postData('rankingListUser', this.userPostData,'polltest/rank/' + this.userPostData.visitor_id).then((result) => {
            this.responseData = result;

            if(this.responseData.status == 'ok') {
                this.rankingUser = JSON.parse(this.responseData.aswer);

                // for(var key in json)
                //   if(json.hasOwnProperty(key))
                //     this.ankiety.push([key, json[key]]); // each item is an array in format [key, value]

                console.log(this.rankingUser);

            }
        }, (err) => {
            console.log('Error' + err);
        });

    }

}
