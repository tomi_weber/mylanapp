import { Component } from '@angular/core';
import {Events, IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {AnkietaPage} from "../ankieta/ankieta";
import {KtoPage} from "../kto/kto";
import {AgendaPage} from "../agenda/agenda";
import {MmRankingPage} from "./tabs/ranking";
import {MmKryteriaPage} from "./tabs/kryteria";
import {MmNagrodyPage} from "./tabs/nagrody";

/**
 * Generated class for the MmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-mm',
  templateUrl: 'mm.html',
})
export class MmPage {

    constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams, public events: Events) {
        this.events.publish('hideHeader', { isHidden: false});
        this.events.publish('enterMM', { show: false});
    }

    goToNav(action){

        if(action == 'ranking') {
            this.navCtrl.push(MmRankingPage);
        }
        if(action == 'kryteria') {
            this.navCtrl.push(MmKryteriaPage);
        }
        if(action == 'nagrody') {
            this.navCtrl.push(MmNagrodyPage);
        }

        this.viewCtrl.dismiss();
    }

}
