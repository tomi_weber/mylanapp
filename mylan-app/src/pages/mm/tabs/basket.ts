import {Component} from '@angular/core';
import {AlertController, Events, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {AuthService} from "../../../providers/auth-service/auth-service";
import {MmPage} from "../mm";

@Component({
    selector: 'page-mm-basket',
    templateUrl: 'basket.html',
})
export class MmBasketPage {

    orders = [];
    ordersSum = 0;
    responseData: any;
    userData = {visitor_id: '', points: 0, visitor_number: ''};

    constructor(public navCtrl: NavController,
                public viewCtrl: ViewController,
                public navParams: NavParams,
                public events: Events,
                public toastController: ToastController,
                public alertController: AlertController,
                public authService: AuthService) {

        this.events.publish('enterMM', { show: true});

        if (localStorage.getItem('basketData')) {
            const data = JSON.parse(localStorage.getItem('basketData'));
            if (data != null) {

                this.ordersSum = data.itemSum;
                this.orders = data.items;
            }
        }

        const data2 = JSON.parse(localStorage.getItem('userData'));

        this.userData.visitor_number = data2.visitor_number;
        this.userData.points = data2.visitor_points;
        this.userData.visitor_id = data2.visitor_id;
    }


    ionViewDidLoad() {
        console.log('ionViewDidLoad MmPage');
    }

    removePosition(element) {

        this.orders = this.orders.filter(item => item.id !== element.id);
        localStorage.setItem('basketData', JSON.stringify(
            {
                itemSum: this.ordersSum - element.amount,
                items: this.orders
            }
        ));

        const toast = this.toastController.create({
            message: 'Produkt został usunięy z koszyka',
            duration: 2000
        });
        toast.present();

    }

    makeOrder() {

        const order: any = {};
        order.user_number = this.userData.visitor_number;
        order.orders = [];

        for (let item of this.orders) {
            order.orders.push({
                product_id: item.product.mm_product_id,
                amount: item.amount,
                sum: item.product.product_points * item.amount
            });
        }

        this.authService.postData('orderSend', order, 'mm/new-order').then((result) => {
            this.responseData = result;

            if (this.responseData.status == 'ok') {
                const usedPoints = this.responseData.used_points;

                localStorage.setItem('basketData', null);

                const data2 = JSON.parse(localStorage.getItem('userData'));
                data2.visitor_points -= usedPoints;
                localStorage.setItem('userData', JSON.stringify(data2));
                this.orders = [];

                const toast = this.toastController.create({
                    message: 'Zamówienie zostało złożone. Wymienione punkty: ' + usedPoints + '. Dziękujemy.',
                    duration: 2000
                });
                toast.present();

            }
        }, (err) => {

        });

        this.backToMm();

    }

    backToMm() {
        this.navCtrl.push(MmPage);
        this.viewCtrl.dismiss();
    }

}
