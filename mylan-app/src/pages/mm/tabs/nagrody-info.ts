import { Component } from '@angular/core';
import {
    NavController, App, ViewController, PopoverController, NavParams, ToastController,
    AlertController
} from 'ionic-angular';
import { AuthService } from '../../../providers/auth-service/auth-service';
import * as $ from 'jquery';
import { Events } from 'ionic-angular';
import {MmPage} from "../mm";

@Component({
    selector: 'page-mm-nagrody-info',
    template: `
        <div class="b-main is-precart" style="text-align: center; background: rgba(0, 0, 0, 0.85)">
            <a (click)="close()" class="a-btn is-primary">
                Zamknij
            </a>
                <div class="b-precart">
                    <div class="b-precart_img">
                        <span class="is-info">{{ product.product_title }}<span>{{ product.product_points }} pkt.</span></span>
                        <img src="http://api-ios.test001.pl/data/product_img/{{ product.mm_product_id }}_thumb.jpg?v={{ dateCache }}" alt="">
                    </div>
                    <div class="b-precart_inner">
                        <div class="b-precart_data">
                            {{ product.category_name }}
                        </div>
                        <div class="b-precart_desc" [innerHTML]="product.product_opis | safeHtml">
                            {{ product.product_opis }}
                            
                            <div class="b-precart_footer">
                                <div class="is-changeCount">
                                    <span class="is-plus" (click)="addAmount()">+</span>
                                    <span class="is-count">{{ amount }}</span>
                                    <span class="is-minus" (click)="removeAmount()">-</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a *ngIf="this.user.points >= this.siteData.limitPunktow || this.user.limitActive == 0 && (!this.user.asm && !this.user.su)" (click)="addToOrder(product)" class="a-btn is-primary">
                    Dodaj do zamówienia
                </a>
                <p *ngIf="this.user.points < this.siteData.limitPunktow && this.user.limitActive == 1 && (!this.user.asm && !this.user.su)"  class="a-typo is-text is-dark" style="color: #fff">Do wymiany pkt na nagrody brakuje: <strong>{{ this.siteData.limitPunktow-this.user.points }} pkt.</strong></p>
        </div>
  `
})

export class MmProductInfoPage {

    product: any;
    user: any;
    amount: number = 1;
    siteData = {ankietyName: '', ankietyActive: false, limitPunktow: 0};
    dateCache: Number;

    constructor(public viewCtrl: ViewController,
                public navParams: NavParams,
                public toastController: ToastController,
                public alertController: AlertController) {
        this.product = this.navParams.get('product');
        this.user = this.navParams.get('user');

        if(localStorage.getItem('siteData')) {
            this.siteData = JSON.parse(localStorage.getItem('siteData'));
        }

        this.dateCache = Date.now();
    }

    close() {
        this.viewCtrl.dismiss();
    }

    public addToOrder(product) {

        let itemsSum = 0;
        let items = [];

        if(localStorage.getItem('basketData')) {
            const data = JSON.parse(localStorage.getItem('basketData'));
            if(data != null) {

                itemsSum = data.itemSum;
                items = data.items;
            }
        }

        let ordersSumPrice = 0;
        if(items.length > 0) {
            for(let item of items) {
                ordersSumPrice += item.product.product_points*item.amount;
            }
        }

        if((ordersSumPrice+(this.amount*product.product_points)) <= this.user.points && this.user.points > 0) {
            itemsSum += this.amount;
            items.push({product: product, amount: this.amount, id: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)});

            localStorage.setItem('basketData', JSON.stringify(
                {
                    itemSum: itemsSum,
                    items: items
                }
            ));

            const toast = this.toastController.create({
                message: 'Produkt został dodany do koszyka',
                duration: 2000
            });
            toast.present();
        } else {

            const alert = this.alertController.create({
                title: 'Uwaga',
                subTitle: 'Produkt nie został dodany do koszyka - brak wystarczającej liczby punktów',
                message: 'Wartość punktowa zamawianych produktów przekroczyła ilość możliwych do użycia punktów.',
                buttons: ['OK']
            });

            alert.present();
        }

        this.viewCtrl.dismiss();
    }

    removeAmount() {
        this.amount--;
        if(this.amount < 1) {
            this.amount = 1;
        }
    }

    addAmount() {
        this.amount++;
    }
}
