import { Component } from '@angular/core';
import { NavController, App, ViewController } from 'ionic-angular';
import { AuthService } from '../../../providers/auth-service/auth-service';
import * as $ from 'jquery';
import { Events } from 'ionic-angular';
import {MmPage} from "../mm";

@Component({
  selector: 'page-mm-kryteria',
  templateUrl: 'kryteria.html'
})
export class MmKryteriaPage {
//m123451
  userPostData = {"visitor_id":""};
  responseData: any;
  agenda: any[] = [];
  kryteria_desc = '';

  constructor( public viewCtrl: ViewController,
               public navCtrl: NavController,
               public authService:AuthService,
               public app: App, public events: Events) {

      this.events.publish('enterMM', { show: true});

      this.authService.postData('nagrodyList', this.agenda, 'mm/kryteria').then((result) => {
          this.responseData = result;

          if (this.responseData.status == 'ok') {
              this.kryteria_desc = JSON.parse(this.responseData.html);
          }
      }, (err) => {

      });

  }

    backToMm() {
        this.navCtrl.push(MmPage);
        this.viewCtrl.dismiss();
    }


}
