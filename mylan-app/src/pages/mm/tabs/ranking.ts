import {Component, ViewChild} from '@angular/core';
import {
    NavController,
    App,
    ViewController,
    ModalController,
    PopoverController,
    AlertController,
    NavParams, ToastController
} from 'ionic-angular';
import {AuthService} from '../../../providers/auth-service/auth-service';
import * as $ from 'jquery';
import {Events} from 'ionic-angular';
import {MmPage} from "../mm";
import {MmNagrodyPage} from "./nagrody";
import {MmProductInfoPage} from "./nagrody-info";
import {cloneDeep} from 'lodash';

@Component({
    selector: 'page-mm-ranking',
    templateUrl: 'ranking.html'
})

export class MmRankingPage {
//m123451

    siteData = {ankietyName: '', ankietyActive: false, limitPunktow: 0, digitalBadgeText: "", digitalBadgeLogo: ""};
    userData = {
        visitor_id: '',
        points: 0,
        ranking: 0,
        rankingPoints: 0,
        visitor_number: '',
        visitor_line: '',
        username: '',
        limitActive: 1,
        rankingLine: '',
        rankingRep: ''
    };

    responseData: any;
    rankingRep: any[] = [];
    rankingAsm: any[] = [];
    rankingLine: any[] = [];
    rankingToView: any[] = [];
    asmList: any[] = [];
    lineList: any[] = [];
    searchString: any = '';

    asm: Boolean = false;
    su: Boolean = false;
    showPoints: Boolean = false;
    asmLine = '';
    rankingTitle = '';

    public selectedAsm = '';
    public selectedAsmInfo = {};
    public selectedLine = '';
    public selectedSort = '0';

    public selectedFilter = '0';

    constructor(public viewCtrl: ViewController,
                public navCtrl: NavController,
                public authService: AuthService,
                public app: App, public events: Events,
                public popoverCtrl: ModalController) {

        this.events.publish('enterMM', { show: true});

        const data = JSON.parse(localStorage.getItem('userData'));
        this.userData.visitor_id = data.visitor_id;
        this.userData.points = data.visitor_points;
        this.userData.visitor_number = data.visitor_number;
        this.userData.username = data.username;
        this.userData.visitor_line = data.visitor_line;
        this.userData.limitActive = data.limitActive;

        if(this.userData.visitor_line.indexOf('ASM') > -1) {
            this.asm = true;
            this.selectedAsm = this.userData.visitor_number;
        } else if(this.userData.visitor_line.indexOf('SU') > -1) {
            this.su = true;
        }

        if(localStorage.getItem('siteData')) {
            this.siteData = JSON.parse(localStorage.getItem('siteData'));
        }

        this.authService.postData('rankingList', this.userData, 'mm/ranking').then((result) => {
            this.responseData = result;

            if (this.responseData.status == 'ok') {
                this.rankingRep = JSON.parse(this.responseData.rankingRep);
                this.rankingAsm = JSON.parse(this.responseData.rankingAsm);
                this.rankingLine = JSON.parse(this.responseData.rankingLine);
                this.asmList = JSON.parse(this.responseData.asmList);
                this.lineList = JSON.parse(this.responseData.lineList);
                this.rankingTitle = this.responseData.rankingTitle;

                if (this.asm) {
                    this.selectedAsmInfo = this.asmList.filter(item => item.visitor_number == this.selectedAsm)[0];
                    for (let item of this.rankingAsm) {
                        if (item.mm_user_number == this.userData.visitor_number) {
                            this.userData.rankingLine = item.mm_position_line;
                            this.userData.ranking = item.mm_position;
                            this.asmLine = item.mm_user_line.toUpperCase();
                            this.userData.rankingPoints = item.mm_points;
                        }
                    }
                }

                if(!this.asm && !this.su) {
                    for (let item of this.rankingRep) {
                        if (item.mm_user_number == this.userData.visitor_number) {
                            this.userData.ranking = item.mm_position;
                            this.userData.rankingPoints = item.mm_points;
                        }
                    }
                }
            }

            this.showRanking();
        }, (err) => {

        });



        events.subscribe('setFilters', (obj) => {
            this.selectedAsm = obj.asm;
            this.selectedLine = obj.line;
            this.selectedSort = obj.sort;
            this.selectedAsmInfo = obj.asmInfo;
            this.searchString = obj.searchString;
            this.showRanking();
        });

        events.subscribe('setView', (obj) => {
            this.selectedFilter = obj.view;
            switch(this.selectedFilter) {
                case '0':
                    this.showRanking();
                    break;
                case '1':
                    this.rankingToView = [...this.rankingAsm];
                    break;
                case '2':
                    this.rankingToView = [...this.rankingLine];
                    break;
            }
        });
    }

    clearView() {
        this.selectedFilter = '0';
        this.showRanking();
    }

    clearFilter(prefix) {
        switch(prefix) {
            case 'asm':
                this.selectedAsm = '';
                this.selectedAsmInfo = {};
                break;
            case 'linia':
                this.selectedLine = '';
                break;
            case 'sort':
                this.selectedSort = '0';
                break;
            case 'search':
                this.searchString = '';
                break;
        }
        this.showRanking();
    }

    showRanking() {
        this.showPoints = true;
        this.rankingToView = [...this.rankingRep];
        if(this.selectedAsm != '') {
            this.rankingToView = this.rankingToView.filter(item => item.mm_asm_number.toLowerCase() == this.selectedAsm.toLowerCase());
        }
        if(this.selectedLine != '') {
            this.rankingToView = this.rankingToView.filter(item => item.mm_user_line.toLowerCase() == this.selectedLine.toLowerCase());
        }
        if(this.selectedSort != '0') {
            switch(this.selectedSort) {
                case '1':
                    this.rankingToView.sort(this.compareValues('mm_position', 'desc'));
                    break;
                case '2':
                    this.rankingToView.sort(this.compareValues('visitor_lastname', 'desc'));
                    break;
                case '3':
                    this.rankingToView.sort(this.compareValues('visitor_lastname'));
                    break;
            }
        }
        if(this.searchString != '') {
            this.rankingToView = this.rankingToView.filter(item => item.visitor_lastname.toLowerCase().includes(this.searchString.toLowerCase()) || item.visitor_firstname.toLowerCase().includes(this.searchString.toLowerCase()));
        }
    }

    compareValues(key, order='asc') {
        return function(a, b) {

            if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
                return 0;
            }
            const varA = (typeof a[key] === 'string') ?
                a[key].toUpperCase() : a[key];
            const varB = (typeof b[key] === 'string') ?
                b[key].toUpperCase() : b[key];

            let comparison = 0;
            if (varA > varB) {
                comparison = 1;
            } else if (varA < varB) {
                comparison = -1;
            }
            return (
                (order == 'desc') ? (comparison * -1) : comparison
            );
        }
    }

    backToMm() {
        this.navCtrl.push(MmPage);
        this.viewCtrl.dismiss();
    }

    goToNagrody() {
        this.navCtrl.push(MmNagrodyPage);
        this.viewCtrl.dismiss();
    }

    filtruj() {
        let popover = this.popoverCtrl.create(MmRankingFiltryPage, {
            selectedAsm: this.selectedAsm,
            selectedLine: this.selectedLine,
            selectedSort: this.selectedSort,
            asmList: this.asmList,
            lineList: this.lineList,
            searchString: this.searchString
        });
        popover.present();
    }

    szukaj() {
        let popover = this.popoverCtrl.create(MmRankingSzukajPage, {
            selectedAsm: this.selectedAsm,
            selectedLine: this.selectedLine,
            selectedSort: this.selectedSort,
            asmList: this.asmList,
            lineList: this.lineList,
            searchString: this.searchString
        });
        popover.present();
    }

    changeView() {
        let popover = this.popoverCtrl.create(MmRankingWidokPage, {
            selectedFilter: this.selectedFilter
        });
        popover.present();
    }

}

@Component({
    selector: 'page-mm-ranking-filtry',
    templateUrl: 'ranking-filtry.html'
})
export class MmRankingFiltryPage {

    asmList = [];
    lineList = [];

    selectedAsm = '';
    selectedLine = '';
    selectedSort = '0';
    searchString = '';

    constructor(public viewCtrl: ViewController,
                public navParams: NavParams, public events: Events) {

        this.events.publish('enterMM', { show: true});

        this.asmList = this.navParams.get('asmList');
        this.lineList = this.navParams.get('lineList');

        this.selectedAsm = this.navParams.get('selectedAsm');
        this.selectedLine = this.navParams.get('selectedLine');
        this.selectedSort = this.navParams.get('selectedSort');
        this.searchString = this.navParams.get('searchString');

    }

    selectFilters($event) {
        this.events.publish('setFilters', {asm: this.selectedAsm, asmInfo: this.asmList.filter(item => item.visitor_number == this.selectedAsm)[0], line: this.selectedLine, sort: this.selectedSort, searchString: this.searchString});
        this.viewCtrl.dismiss();
    }

}

@Component({
    selector: 'page-mm-ranking-szukaj',
    templateUrl: 'ranking-szukaj.html'
})
export class MmRankingSzukajPage {

    asmList = [];
    lineList = [];

    selectedAsm = '';
    selectedLine = '';
    selectedSort = '0';
    searchString = '';

    constructor(public viewCtrl: ViewController,
                public navParams: NavParams, public events: Events) {

        this.events.publish('enterMM', { show: true});

        this.asmList = this.navParams.get('asmList');
        this.lineList = this.navParams.get('lineList');

        this.selectedAsm = this.navParams.get('selectedAsm');
        this.selectedLine = this.navParams.get('selectedLine');
        this.selectedSort = this.navParams.get('selectedSort');
        this.searchString = this.navParams.get('searchString');

    }

    selectFilters($event) {
        this.events.publish('setFilters', {asm: this.selectedAsm, asmInfo: this.asmList.filter(item => item.visitor_number == this.selectedAsm)[0], line: this.selectedLine, sort: this.selectedSort, searchString: this.searchString});
        this.viewCtrl.dismiss();
    }

}

@Component({
    selector: 'page-mm-ranking-widok',
    templateUrl: 'ranking-widok.html'
})
export class MmRankingWidokPage {

    selectedFilter = '0';

    constructor(public viewCtrl: ViewController,
                public navParams: NavParams, public events: Events) {

        this.events.publish('enterMM', { show: true});
        this.selectedFilter = this.navParams.get('selectedFilter');
    }

    changeView($event) {
        this.events.publish('setView', {view: this.selectedFilter});
        this.viewCtrl.dismiss();
    }

}
