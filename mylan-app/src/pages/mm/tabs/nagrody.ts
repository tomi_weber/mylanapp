import {Component} from '@angular/core';
import {NavController, App, ViewController, PopoverController, NavParams, ModalController} from 'ionic-angular';
import {AuthService} from '../../../providers/auth-service/auth-service';
import * as $ from 'jquery';
import {Events} from 'ionic-angular';
import {MmPage} from "../mm";
import {MmProductInfoPage} from "./nagrody-info";
import {MmBasketPage} from "./basket";


@Component({
    selector: 'page-mm-nagrody',
    templateUrl: 'nagrody.html'
})
export class MmNagrodyPage {
//m123451
    userData = {visitor_id: '', points : 0, visitor_number: '', limitActive : 1, visitor_line: '', asm: false, su: false};
    responseData: any;
    produkty: any[] = [];

    objectKeys = Object.keys;
    dateCache: Number;

    constructor(public viewCtrl: ViewController,
                public navCtrl: NavController,
                public authService: AuthService,
                public app: App, public events: Events,
                public modalCtrl: ModalController) {

        this.events.publish('enterMM', { show: true});

        const data2 = JSON.parse(localStorage.getItem('userData'));

        this.userData.visitor_number = data2.visitor_number;
        this.userData.points = data2.visitor_points;
        this.userData.visitor_id = data2.visitor_id;
        this.userData.visitor_line = data2.visitor_line;
        this.userData.limitActive = data2.limitActive;

        if(this.userData.visitor_line.indexOf('ASM') > -1) {
            this.userData.asm = true;
        } else if(this.userData.visitor_line.indexOf('SU') > -1) {
            this.userData.su = true;
        }

        this.dateCache = Date.now();

        this.authService.postData('nagrodyList', this.userData, 'mm/product-list').then((result) => {
            this.responseData = result;

            if (this.responseData.status == 'ok') {
                var json = JSON.parse(this.responseData.productList);

                for (var key in json)
                    if (json.hasOwnProperty(key))
                        this.produkty.push([key, json[key]]); // each item is an array in format [key, value]


            }
        }, (err) => {

        });

    }

    presentPopover(product) {
        let popover = this.modalCtrl.create(MmProductInfoPage, {product: product, user: this.userData});
        popover.present();
    }

    backToMm() {
        this.navCtrl.push(MmPage);
        this.viewCtrl.dismiss();
    }

    goToCard() {
        this.navCtrl.push(MmBasketPage);
        this.viewCtrl.dismiss();
    }

    public countBasketItems() {
        if (localStorage.getItem('basketData')) {
            const data = JSON.parse(localStorage.getItem('basketData'));
            if (data != null) {
                return data.itemSum;
            }
        }

        return 0;
    }

}