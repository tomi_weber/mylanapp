import {Component} from '@angular/core';
import {NavController, App, ViewController} from 'ionic-angular';
import {AuthService} from '../../providers/auth-service/auth-service';
import {AlertController} from 'ionic-angular';
import {WelcomePage} from "../welcome/welcome";
import {Events} from 'ionic-angular';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class Login {

    responseData: any;
    userData = {visitor_number: "", visitor_pass: ""};

    constructor(public navCtrl: NavController,
                public viewCtrl: ViewController,
                public authService: AuthService,
                public app: App,
                private alertCtrl: AlertController,
                public events: Events) {

        this.events.publish('hideHeader', {isHidden: true});
        this.events.publish('enterMM', {show: false});

        if (localStorage.getItem('userData')) {
            const data = JSON.parse(localStorage.getItem('userData'));
            if (data != null && data.visitor_id != '') {
                this.events.publish('showMM', { show: data.mm});
                this.viewCtrl.dismiss();
                this.navCtrl.push(WelcomePage);
            }
        }

    }

    login() {
        console.log('login method');

        this.authService.postData('loginSite', this.userData, 'visitor/check').then((result) => {
            this.responseData = result;

            if (this.responseData.status == 'ok') {
                localStorage.setItem('userData', JSON.stringify(this.responseData));
                this.events.publish('showMM', { show: this.responseData.mm});

                this.navCtrl.push(WelcomePage);
                this.viewCtrl.dismiss();


            } else {
                //błąd logowania
                this.presentAlert();
            }
        }, (err) => {
            console.log('Error' + err);
        });
    }

    presentAlert() {
        let alert = this.alertCtrl.create({
            title: 'Numer lub hasło niepoprawne',
            subTitle: 'Wprowadź poprawny numer zaczynający się od "M..."',
            buttons: ['Zamknij']
        });
        alert.present();
    }


}