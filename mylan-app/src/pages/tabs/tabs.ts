import { Component } from '@angular/core';
import { NavController, App, ViewController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';

import { AgendaPage } from '../agenda/agenda';
import { AnkietaPage } from '../ankieta/ankieta';
import { KtoPage } from '../kto/kto';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  constructor(public authService: AuthService,
              public viewCtr: ViewController,
              public navCtr: NavController,
              public app: App) {

  }

   goToNav(action){

        if(action == 'tabAgenda') {
            //var model = new AgendaPage(this.viewCtr, this.navCtr, this.authService, this.app);
            //model.goToNav(action);
        }

    }
}
