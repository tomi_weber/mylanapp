import {Component} from '@angular/core';
import {NavController, App, ViewController} from 'ionic-angular';
import {AuthService} from '../../providers/auth-service/auth-service';
import {AnkietaPage} from '../ankieta/ankieta';
import {Login} from '../login/login';
import {KtoPage} from '../kto/kto';
import * as $ from 'jquery';
import {Events} from 'ionic-angular';

@Component({
    selector: 'page-agenda',
    templateUrl: 'agenda.html'
})
export class AgendaPage {
//m123451
    userPostData = {"visitor_id": ""};
    responseData: any;
    agenda: any[] = [];

    constructor(public viewCtrl: ViewController,
                public navCtrl: NavController,
                public authService: AuthService,
                public app: App, public events: Events) {

        this.events.publish('hideHeader', {isHidden: false});
        this.events.publish('enterMM', {show: false});

        const data = JSON.parse(localStorage.getItem('userData'));
        this.userPostData.visitor_id = data.visitor_id;

        this.authService.postData('agendaList', this.userPostData, 'agenda/get-list/').then((result) => {
            this.responseData = result;

            if (this.responseData.status == 'ok') {
                var json = JSON.parse(this.responseData.agenda);

                for (var key in json)
                    if (json.hasOwnProperty(key))
                        this.agenda.push([key, json[key]]); // each item is an array in format [key, value]

            }
        }, (err) => {

        });

    }

    toggleBox(id) {
        $('#box_full_' + id).toggle();
        $('#box_short_' + id).toggle();
        $('#click_full_' + id).parent().parent().parent().toggleClass('is-active');
        $('#click_full_' + id).toggleClass('icon-more01');
    }

    toggleSection(i) {
        if (!this.agenda[i][1]['active']) {
            for (let a of this.agenda) {
                a[1]['active'] = false;
            }
            ;
        }
        this.agenda[i][1]['active'] = !this.agenda[i][1]['active'];
    }

    goToNav(action) {

        if (action == 'tabAgenda') {
            this.navCtrl.push(AgendaPage);
        }
        if (action == 'tabAnkieta') {
            this.navCtrl.push(AnkietaPage);
        }
        if (action == 'tabKto') {
            this.navCtrl.push(KtoPage);
        }

        this.viewCtrl.dismiss();
    }

    logout() {
        localStorage.setItem('userData', null);
        this.navCtrl.push(Login);
        this.viewCtrl.dismiss();
    }


}
