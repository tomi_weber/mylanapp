import {Component} from '@angular/core';
import {NavController, App, ViewController} from 'ionic-angular';
import {AuthService} from '../../providers/auth-service/auth-service';
import {Login} from '../login/login';
import {AgendaPage} from '../agenda/agenda';
import {AnkietaPage} from '../ankieta/ankieta';
import {Events} from 'ionic-angular';

@Component({
    selector: 'page-kto',
    templateUrl: 'kto.html'
})
export class KtoPage {

    responseData: any;
    whoiswho: any[] = [];

    userPostData = {"comapny_id": "", "comapny_account": "", "company_name": "", "place": "", "status": ""};

    constructor(public navCtrl: NavController,
                public viewCtrl: ViewController,
                public authService: AuthService,
                public app: App, public events: Events) {
        this.events.publish('hideHeader', {isHidden: false});
        this.events.publish('enterMM', {show: false});

        this.getData();
    }

    public getData() {

        this.authService.postData('ktoList', this.userPostData, 'visitor/whoiswho').then((result) => {
            this.responseData = result;

            if (this.responseData.status == 'ok') {
                var json = JSON.parse(this.responseData.userData);

                for (var key in json)
                    if (json.hasOwnProperty(key))
                        this.whoiswho.push([key, json[key]]); // each item is an array in format [key, value]
            }
        }, (err) => {

        });
    }

}