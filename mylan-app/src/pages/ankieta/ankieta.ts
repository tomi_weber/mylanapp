import {Component} from '@angular/core';
import {NavController, App, ViewController} from 'ionic-angular'
import {AuthService} from '../../providers/auth-service/auth-service';
import {Login} from '../login/login';
import {AgendaPage} from '../agenda/agenda';
import {AlertController, MenuController} from 'ionic-angular';
import {KtoPage} from '../kto/kto';
import * as $ from 'jquery';
import {Events} from 'ionic-angular';

@Component({
    selector: 'page-ankieta',
    templateUrl: 'ankieta.html'
})
export class AnkietaPage {

    userPostData = {"visitor_id": "", "answer_id": []};
    pollAnswer: any = {visitor_id: "", answers: []};
    responseData: any;
    ankiety: any[] = [];

    constructor(public navCtrl: NavController,
                public viewCtrl: ViewController,
                public authService: AuthService,
                public app: App,
                private alertCtrl: AlertController,
                menu: MenuController, public events: Events) {
        this.events.publish('hideHeader', {isHidden: false});
        this.events.publish('enterMM', {show: false});
        menu.enable(true);

        this.getData();

    }

    public getData() {

        this.ankiety = [];

        const data = JSON.parse(localStorage.getItem('userData'));
        this.userPostData.visitor_id = data.visitor_id;
        this.pollAnswer.visitor_id = data.visitor_id;

        this.authService.postData('ankietaList', this.userPostData, 'poll/get').then((result) => {
            this.responseData = result;

            if (this.responseData.status == 'ok') {
                this.ankiety = JSON.parse(this.responseData.polls);

                for (let item in this.ankiety) {
                    this.pollAnswer.answers.push(0);
                }
                console.log(this.ankiety);

            }
        }, (err) => {
            console.log('Error' + err);
        });
    }

    public odswiez() {

        this.getData();
    }

    start(poll_id) {

        this.authService.postData('ankietaStart', this.userPostData, 'poll/activate/' + poll_id).then((result) => {
            this.responseData = result;
            if (this.responseData.status == 'ok') {
                this.navCtrl.push(AnkietaPage);
                this.viewCtrl.dismiss();
            }
        }, (err) => {
            console.log('Error' + err);
        });
    }

    stop(poll_id) {

        this.authService.postData('ankietaStop', this.userPostData, 'poll/deactivate/' + poll_id).then((result) => {
            this.responseData = result;
            if (this.responseData.status == 'ok') {
                this.navCtrl.push(AnkietaPage);
                this.viewCtrl.dismiss();
            }
        }, (err) => {
            console.log('Error' + err);
        });
    }

    results(poll_id) {

        $('#results_' + poll_id).slideToggle();
    }

    vote() {
        this.authService.postData('ankietaVote', this.pollAnswer, 'poll/vote').then((result) => {
            this.responseData = result;
            if (this.responseData.status == 'ok') {
                //this.presentAlert();
                this.navCtrl.push(AnkietaThanksPage);
                this.viewCtrl.dismiss();
            } else {
                alert('Wystąpił błąd podczas oddawania głosu.');
            }
        }, (err) => {
            console.log('Error' + err);
        });
    }

    goToNav(action) {
        if (action == 'tabAgenda') {
            this.navCtrl.push(AgendaPage);
        }
        if (action == 'tabAnkieta') {
            this.navCtrl.push(AnkietaPage);
        }
        if (action == 'tabKto') {
            this.navCtrl.push(KtoPage);
        }

        this.viewCtrl.dismiss();

    }


    presentAlert() {
        let alert = this.alertCtrl.create({
            title: 'Dziękujemy za Twój głos',
            subTitle: '',
            buttons: ['Zamknij']
        });
        alert.present();
    }


}

@Component({
    selector: 'page-ankieta-thanks',
    templateUrl: 'ankieta-thanks.html'
})
export class AnkietaThanksPage {

    constructor(public navCtrl: NavController,
                public viewCtrl: ViewController,
                public authService: AuthService,
                public app: App,
                private alertCtrl: AlertController,
                menu: MenuController, public events: Events) {
        this.events.publish('hideHeader', {isHidden: false});
        this.events.publish('enterMM', {show: false});

        menu.enable(true);

    }

    back() {
        this.navCtrl.push(AnkietaPage);
    }

}
