import {Component} from '@angular/core';
import {NavController, ToastController, ViewController, LoadingController, Platform} from 'ionic-angular';
import {Events} from 'ionic-angular';

import {AgendaPage} from '../agenda/agenda';
import {AnkietaPage} from '../ankieta/ankieta';
import {KtoPage} from '../kto/kto';
import {NoclegiPage} from '../noclegi/noclegi';
import {NewsPage} from '../news/news';
import {ScorePage} from '../score/score';
import {TestPage} from '../test/test';
import {MmPage} from "../mm/mm";
import {DigitalBadgePage} from "../digitalbadge/digitalbadge";

@Component({
    selector: 'page-welcome',
    templateUrl: 'welcome.html',
})
export class WelcomePage {
    public siteData = {ankietyName: '', ankietyActive: false, limitPunktow: 0, digitalBadgeText: "", digitalBadgeLogo: "", digitalBadgeVisible: 0};
    userPostData = {visitor_id: "", mm:  false};

    constructor(private toast: ToastController,
                public viewCtrl: ViewController,
                public navCtrl: NavController,
                public loadingCtrl: LoadingController,
                public events: Events,
                public platform: Platform) {
        this.events.publish('hideHeader', {isHidden: true});
        this.events.publish('enterMM', {show: false});

        if (localStorage.getItem('userData')) {
            const data = JSON.parse(localStorage.getItem('userData'));

            if (data != null) {
                this.userPostData.visitor_id = data.visitor_id;
                this.userPostData.mm = data.mm;
                //var tags = data.visitor_line.split(',');
                /*window["plugins"].OneSignal
                      .sendTags({district: data.visitor_district, visitor_function: data.visitor_function, visitor_id: data.visitor_id});

                  for(var i=0;i<tags.length;i++) {
                      window["plugins"].OneSignal.sendTag("line" + i, tags[i]);
                  }*/
            }
        }

        if (localStorage.getItem('siteData')) {
            this.siteData = JSON.parse(localStorage.getItem('siteData'));
        }

    }

    ionViewWillEnter() {
        this.events.publish('root:created', Date.now());
    }

    public presentLoadingDefault() {
        let loading = this.loadingCtrl.create({
            content: 'Proszę czekać...'
        });

        loading.present();

        setTimeout(() => {
            loading.dismiss();
        }, 1000);
    }

    goToNav(action) {
        if (action == 'tabAgenda') {
            //this.presentLoadingDefault();
            this.navCtrl.push(AgendaPage);
            this.viewCtrl.dismiss();
            this.events.publish('setSiteTitle', {title: 'Agenda'});
            this.events.publish('hideHeader', {isHidden: false});
        }
        if (action == 'tabAnkieta') {
            //this.presentLoadingDefault();
            this.navCtrl.push(AnkietaPage);
            this.viewCtrl.dismiss();
            this.events.publish('setSiteTitle', {title: 'Ankiety'});
            this.events.publish('hideHeader', {isHidden: false});
        }
        if (action == 'tabKto') {
            //this.presentLoadingDefault();
            this.navCtrl.push(KtoPage);
            this.viewCtrl.dismiss();
            this.events.publish('setSiteTitle', {title: 'Kto jest kto'});
            this.events.publish('hideHeader', {isHidden: false});
        }
        if (action == 'tabNoclegi') {
            //this.presentLoadingDefault();
            this.navCtrl.push(NoclegiPage);
            this.viewCtrl.dismiss();
            this.events.publish('setSiteTitle', {title: 'Noclegi'});
            this.events.publish('hideHeader', {isHidden: false});
        }
        if (action == 'tabNewsy') {
            //this.presentLoadingDefault();
            this.navCtrl.push(NewsPage);
            this.viewCtrl.dismiss();
            this.events.publish('setSiteTitle', {title: 'Aktualności'});
            this.events.publish('hideHeader', {isHidden: false});
        }
        if (action == 'tabCele') {
            //this.presentLoadingDefault();
            this.navCtrl.push(ScorePage);
            this.viewCtrl.dismiss();
            this.events.publish('setSiteTitle', {title: 'Cele'});
            this.events.publish('hideHeader', {isHidden: false});
        }
        if (action == 'tabMm') {
            //this.presentLoadingDefault();
            this.navCtrl.push(MmPage);
            this.viewCtrl.dismiss();
            this.events.publish('setSiteTitle', {title: 'm&m'});
            this.events.publish('hideHeader', {isHidden: true});
        }

        if (action == 'tabTest') {
            //this.presentLoadingDefault();
            this.navCtrl.push(TestPage);
            this.viewCtrl.dismiss();
            this.events.publish('setSiteTitle', {title: this.siteData.ankietyName});
            this.events.publish('hideHeader', {isHidden: false});
        }

      if (action == 'tabDigitalBadge') {
        this.presentLoadingDefault();
        this.navCtrl.push(DigitalBadgePage);
        this.viewCtrl.dismiss();
        this.events.publish('setSiteTitle', {title: 'Identyfikator'});
        this.events.publish('hideHeader', {isHidden: false});
      }

    }

    // ionViewDidEnter() {
    //   this.network.onDisconnect().subscribe(data => {
    //     console.log(data)
    //     this.displayNetworkUpdate(data.type);
    //   }, error => console.error(error));
    // }
    //
    // displayNetworkUpdate(connectionState: string){
    //
    //   if(connectionState == 'offline') {
    //     let networkType = this.network.type;
    //     this.toast.create({
    //       message: `Pracujesz w trybie offline!`,
    //       duration: 3000
    //     }).present();
    //   }
    // }
}



