import {Component} from '@angular/core';
import {NavController, App, ViewController} from 'ionic-angular'
import {AuthService} from '../../providers/auth-service/auth-service';
import {AlertController, MenuController} from 'ionic-angular';
import * as $ from 'jquery';
import { Events } from 'ionic-angular';

@Component({
    selector: 'page-noclegi',
    templateUrl: 'noclegi.html'
})
export class NoclegiPage {

    userPostData = {"visitor_id": ""};
    invitePostData = {"visitor_id": "", "user_id": ""};
    responseData: any;
    invites: any[] = [];
    invitorsSelect: any[] = [];

    constructor(public navCtrl: NavController,
                public viewCtrl: ViewController,
                public authService: AuthService,
                public app: App,
                private alertCtrl: AlertController,
                menu: MenuController, public events: Events) {

        menu.enable(true);
        this.events.publish('hideHeader', { isHidden: false});
        const data = JSON.parse(localStorage.getItem('userData'));
        this.userPostData.visitor_id = data.visitor_id;
        this.invitePostData.visitor_id = data.visitor_id;

        this.authService.postData('invoicesList', this.userPostData, 'visitor/getinvite').then((result) => {
            this.responseData = result;

            if (this.responseData.status == 'ok') {
                this.invites = JSON.parse(this.responseData.invites);
                console.log(this.invites);

                //if(this.invites.accepted_active == 0) {
                    this.authService.postData('invitersList', this.userPostData, 'visitor/getinvitorslist').then((result) => {
                        this.responseData = result;
                        if (this.responseData.status == 'ok') {
                            this.invitorsSelect = JSON.parse(this.responseData.invitorsSelect);
                        }
                    }, (err) => {
                        console.log('Error' + err);
                    });
                //}
            }
        }, (err) => {
            console.log('Error' + err);
        });

    }

    accept(request_id) {

        this.authService.postData('inviteAccept', this.userPostData, 'visitor/response-invite/accept/' + request_id).then((result) => {
            this.responseData = result;
            if (this.responseData.status == 'ok') {
                this.navCtrl.push(NoclegiPage);
                this.viewCtrl.dismiss();
            }
        }, (err) => {
            console.log('Error' + err);
        });
    }

    decline(request_id) {

        this.authService.postData('inviteDecline', this.userPostData, 'visitor/response-invite/decline/' + request_id).then((result) => {
            this.responseData = result;
            if (this.responseData.status == 'ok') {
                this.navCtrl.push(NoclegiPage);
                this.viewCtrl.dismiss();
            }
        }, (err) => {
            console.log('Error' + err);
        });
    }

    results(poll_id) {

        $('#results_' + poll_id).slideToggle();
    }

    sendRequest() {
        this.authService.postData('sendRequest', this.invitePostData, 'visitor/addinvite').then((result) => {
            this.responseData = result;
            if (this.responseData.status == 'ok') {
                this.presentAlert();
                this.navCtrl.push(NoclegiPage);
                this.viewCtrl.dismiss();
            } else {
                alert('Wystąpił błąd podczas wysyłania zaproszenia.');
            }
        }, (err) => {
            console.log('Error' + err);
        });
    }

    presentAlert() {
        let alert = this.alertCtrl.create({
            title: 'Twoje zaproszenie zostało wysłane',
            subTitle: '',
            buttons: ['Zamknij']
        });
        alert.present();
    }

    presentAlert2() {
        let alert = this.alertCtrl.create({
            title: 'Twoje zaproszenie zostało wysłane',
            subTitle: '',
            buttons: ['Zamknij']
        });
        alert.present();
    }


}
