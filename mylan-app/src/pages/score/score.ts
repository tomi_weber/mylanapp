import { Component } from '@angular/core';
import { NavController, App, NavParams, ViewController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Events } from 'ionic-angular';


/**
 * Generated class for the ScorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-score',
  templateUrl: 'score.html',
})
export class ScorePage {

    responseData: any;
    scoreList: any[] = [];

    userPostData = {"visitor_id":""};

    constructor(public navCtrl: NavController,
                public viewCtrl: ViewController,
                public authService: AuthService,
                public navParams: NavParams,
                public app: App, public events: Events) {
        this.events.publish('hideHeader', { isHidden: false});
        this.events.publish('enterMM', {show: false});

        const data = JSON.parse(localStorage.getItem('userData'));
        this.userPostData.visitor_id = data.visitor_id;

        this.authService.postData('scoreList', this.userPostData,'news/score/userlist').then((result) => {
            this.responseData = result;

            if(this.responseData.status == 'ok') {
                this.scoreList = JSON.parse(this.responseData.userData);

            }
        }, (err) => {

        });
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScorePage');
  }

}
