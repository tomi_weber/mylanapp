import {Component, ViewChild} from '@angular/core';
import {NavController, App, NavParams, ViewController, Slides, Slide} from 'ionic-angular';
import {AuthService} from '../../providers/auth-service/auth-service';
import {Events} from 'ionic-angular';

@Component({
    selector: 'page-news',
    templateUrl: 'digitalbadge.html'
})
export class DigitalBadgePage {

    responseData: any;
    newsList: any[] = [];
    newsId: any = 0;
    toogled: boolean = false;

    slideOpts = {
        effect: 'flip'
    };

    userData = {
      visitor_id: '',
      points: 0,
      ranking: 0,
      rankingPoints: 0,
      visitor_number: '',
      visitor_line: '',
      username: '',
      limitActive: 1,
      rankingLine: '',
      rankingRep: ''
    };
    public siteData = {ankietyName: '', ankietyActive: false, limitPunktow: 0, digitalBadgeText: "", digitalBadgeLogo: ""};

    userPostData = {"visitor_id": ""};
    @ViewChild(Slides) slides: Slide;

    constructor(public navCtrl: NavController,
                public viewCtrl: ViewController,
                public authService: AuthService,
                public navParams: NavParams,
                public app: App, public events: Events) {
        this.events.publish('hideHeader', {isHidden: false});
        this.events.publish('enterMM', {show: false});

        const data = JSON.parse(localStorage.getItem('userData'));
        this.userData.visitor_id = data.visitor_id;
        this.userData.points = data.visitor_points;
        this.userData.visitor_number = data.visitor_number;
        this.userData.username = data.username;
        this.userData.visitor_line = data.visitor_line;
        this.userData.limitActive = data.limitActive;

        if (localStorage.getItem('siteData')) {
          this.siteData = JSON.parse(localStorage.getItem('siteData'));
        }

        this.siteData.digitalBadgeLogo = "http://api-ios.test001.pl/data/loga/digital_badge_logo." + this.siteData.digitalBadgeLogo;
    }


}
