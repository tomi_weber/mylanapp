import {Component, ViewChild} from '@angular/core';
import {NavController, App, NavParams, ViewController, Slides, Slide} from 'ionic-angular';
import {AuthService} from '../../providers/auth-service/auth-service';
import {Events} from 'ionic-angular';

@Component({
    selector: 'page-news',
    templateUrl: 'news.html'
})
export class NewsPage {

    responseData: any;
    newsList: any[] = [];
    newsId: any = 0;
    toogled: boolean = false;

    slideOpts = {
        effect: 'flip'
    };

    userPostData = {"visitor_id": ""};
    @ViewChild(Slides) slides: Slide;

    constructor(public navCtrl: NavController,
                public viewCtrl: ViewController,
                public authService: AuthService,
                public navParams: NavParams,
                public app: App, public events: Events) {
        this.events.publish('hideHeader', {isHidden: false});
        this.events.publish('enterMM', {show: false});

        const data = JSON.parse(localStorage.getItem('userData'));
        this.userPostData.visitor_id = data.visitor_id;

        this.authService.postData('newsList', this.userPostData, 'news/userlist').then((result) => {
            this.responseData = result;

            if (this.responseData.status == 'ok') {
                this.newsList = JSON.parse(this.responseData.userData);
                if (navParams.get('param_news_id') != null) {
                    this.newsId = navParams.get('param_news_id');
                }

            }
        }, (err) => {

        });
    }

    public executeAfterPush(lastItem: boolean) {
        console.log('LAST item' + lastItem);
        if (lastItem && this.newsId != 0 && !this.toogled) {
            setTimeout(() => {
                //this.toggleBox2(this.newsId);
            }, 1000);
            this.toogled = true;
        }
    }

}