import {Component, ViewChild} from '@angular/core';
import {NavController, App, Platform, MenuController, LoadingController} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Events} from 'ionic-angular';

import {AgendaPage} from '../pages/agenda/agenda';
import {AnkietaPage} from '../pages/ankieta/ankieta';
import {Login} from '../pages/login/login';
import {KtoPage} from '../pages/kto/kto';
import {WelcomePage} from '../pages/welcome/welcome';
import {AuthService} from '../providers/auth-service/auth-service';
import {NoclegiPage} from '../pages/noclegi/noclegi';
import {ScorePage} from '../pages/score/score';
import {TestPage} from '../pages/test/test';
import {NewsPage} from '../pages/news/news';

import {CacheService} from "ionic-cache";
import {MmPage} from "../pages/mm/mm";
import {DigitalBadgePage} from "../pages/digitalbadge/digitalbadge";

@Component({
    selector: 'page-homeapp',
    templateUrl: 'app.html'
})

export class MyApp {
    rootPage: any = Login;
    public footerIsHidden: boolean = false;
    public mmModule: boolean = false;
    public mmModuleMenu: boolean = false;
    public siteTitle: any = 'NSM 2018';
    public siteData = {ankietyName: "", ankietyActive: false, digitalBadgeVisible: 0};
    public notificationOpenedCallback: any;
    responseData: any;
    filter = false;

    @ViewChild('mycontent') nav: NavController

    constructor(public platform: Platform,
                statusBar: StatusBar,
                cache: CacheService,
                splashScreen: SplashScreen,
                public app: App,
                public authService: AuthService,
                public menuCtrl: MenuController,
                public loadingCtrl: LoadingController,
                public events: Events) {
        this.platform.ready().then(() => {


            cache.setDefaultTTL(60 * 60 * 12); //12h cache
            cache.setOfflineInvalidate(false);
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            statusBar.overlaysWebView(false);
            splashScreen.hide();

            this.notificationOpenedCallback = (jsonData: any) => {
                //var notificationOpenedCallback = function(jsonData) {
                console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
                if (jsonData.notification.payload.additionalData != null) {
                    console.log("Here we access addtional data");
                    if (jsonData.notification.payload.additionalData.news_id != null) {
                        //this.goToNav('tabWelcome');
                        console.log('Here we access the news_id sent in the notification data');
                        //events.subscribe('root:created', (data) => {
                        this.goToNav('tabNews', jsonData.notification.payload.additionalData.news_id);
                        //events.unsubscribe('root:created');
                        //});

                    }
                }
            };



            /*  window["plugins"].OneSignal
                  .startInit("73b5d8a1-0f4f-481a-8d2b-cdb8956f9569")
                  .handleNotificationOpened(this.notificationOpenedCallback)
                  .endInit();*/


        });

        this.authService.postData('tabTextSite', {}, 'visitor/check2').then((result) => {
            this.responseData = result;
            localStorage.setItem('siteData', JSON.stringify(this.responseData));
            this.siteData = this.responseData;
        }, (err) => {
            console.log('Error' + err);
        });

        if (localStorage.getItem('userData')) {
            const data = JSON.parse(localStorage.getItem('userData')); console.log('data', data);
            if (data != null && data.visitor_id != '') {
                this.mmModuleMenu = data.mm;
            }
        }

        events.subscribe('hideHeader', (data) => {
            this.footerIsHidden = data.isHidden;
        });

        events.subscribe('enterMM', (data) => {
            this.mmModule = data.show;
        });

        events.subscribe('showMM', (data) => {
            this.mmModuleMenu = data.show;
        });

        events.subscribe('setSiteTitle', (data) => {
            this.siteTitle = data.title;
        });
    }

    public presentLoadingDefault(time = 1000) {
        let loading = this.loadingCtrl.create({
            content: 'Proszę czekać...'
        });

        loading.present();

        setTimeout(() => {
            loading.dismiss();
        }, time);
    }

    public onFilterChange(eve: any) {
        this.filter = !this.filter;
    }

    goToNav(action, news_id = null) {

        this.filter = false;
        if (action == 'tabAgenda') {
            //this.presentLoadingDefault();
            const startIndex = this.nav.getActive().index;

            this.nav.push(AgendaPage).then(() => {
                this.nav.remove(startIndex);
            });
            this.events.publish('setSiteTitle', {title: 'Agenda'});
        }
        if (action == 'tabAnkieta') {
            //this.presentLoadingDefault();
            const startIndex = this.nav.getActive().index;

            this.nav.push(AnkietaPage).then(() => {
                this.nav.remove(startIndex);
            });
            this.events.publish('setSiteTitle', {title: 'Ankiety'});
        }
        if (action == 'tabKto') {
            //this.presentLoadingDefault();
            const startIndex = this.nav.getActive().index;

            this.nav.push(KtoPage).then(() => {
                this.nav.remove(startIndex);
            });
            this.events.publish('setSiteTitle', {title: 'Kto jest kto'});
        }
        if (action == 'tabWelcome') {
            //this.presentLoadingDefault();
            const startIndex = this.nav.getActive().index;

            this.nav.push(WelcomePage).then(() => {
                this.nav.remove(startIndex);
            });
            this.events.publish('setSiteTitle', {title: 'NSM 2018'});
        }
        if (action == 'tabNoclegi') {
            //this.presentLoadingDefault();
            const startIndex = this.nav.getActive().index;

            this.nav.push(NoclegiPage).then(() => {
                this.nav.remove(startIndex);
            });
            this.events.publish('setSiteTitle', {title: 'Noclegi'});
        }
        if (action == 'tabScore') {
            //this.presentLoadingDefault();
            const startIndex = this.nav.getActive().index;

            this.nav.push(ScorePage).then(() => {
                this.nav.remove(startIndex);
            });
            this.events.publish('setSiteTitle', {title: 'Cele'});
        }
        if (action == 'tabTest') {
            //this.presentLoadingDefault();
            const startIndex = this.nav.getActive().index;

            this.nav.push(TestPage).then(() => {
                this.nav.remove(startIndex);
            });
            this.events.publish('setSiteTitle', {title: this.siteData.ankietyName});
        }
        if (action == 'tabMm') {
            //this.presentLoadingDefault();
            const startIndex = this.nav.getActive().index;

            this.nav.push(MmPage).then(() => {
                this.nav.remove(startIndex);
            });
            this.events.publish('setSiteTitle', {title: 'M&M'});
        }
        if (action == 'tabNews') {
            //this.presentLoadingDefault();
            const startIndex = this.nav.getActive().index;

            // news_id = 6;
            if (news_id != null) {
                this.nav.push(NewsPage, {
                    param_news_id: news_id
                }).then(() => {
                    this.nav.remove(startIndex);
                });
            } else {
                this.nav.push(NewsPage).then(() => {
                    this.nav.remove(startIndex);
                });
            }
            this.events.publish('setSiteTitle', {title: 'Aktualności'});
        }
        if (action == 'tabDigitalBadge') {
          this.presentLoadingDefault();
          const startIndex = this.nav.getActive().index;

          this.nav.push(DigitalBadgePage).then(() => {
            this.nav.remove(startIndex);
          });
          this.events.publish('setSiteTitle', {title: 'Identyfikator'});
        }
    }

    logout() {
        this.filter = false;
        localStorage.setItem('userData', null);

        const startIndex = this.nav.getActive().index;

        this.nav.push(Login).then(() => {
            this.nav.remove(startIndex);
        });
    }
}
