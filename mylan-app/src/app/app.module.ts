import {NgModule, ErrorHandler} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';

import {MyApp} from './app.component';
import {AuthService} from '../providers/auth-service/auth-service';
import {WelcomePage} from '../pages/welcome/welcome';
import {Login} from '../pages/login/login';
import {AgendaPage} from '../pages/agenda/agenda';
import {AnkietaPage, AnkietaThanksPage} from '../pages/ankieta/ankieta';
import {KtoPage} from '../pages/kto/kto';
import {NoclegiPage} from '../pages/noclegi/noclegi';
import {ScorePage} from '../pages/score/score';
import {TestPage} from '../pages/test/test';
import {NewsPage} from '../pages/news/news';
import {TabsPage} from '../pages/tabs/tabs';
import {Page1Page} from '../pages/test/page1';
import {Page2Page} from '../pages/test/page2';
import {Page3Page} from '../pages/test/page3';
import {MmPage} from '../pages/mm/mm';
import {MmKryteriaPage} from '../pages/mm/tabs/kryteria';
import {MmNagrodyPage} from '../pages/mm/tabs/nagrody';
import {MmProductInfoPage} from '../pages/mm/tabs/nagrody-info';
import {MmRankingPage, MmRankingFiltryPage, MmRankingWidokPage, MmRankingSzukajPage} from '../pages/mm/tabs/ranking';
import {MmBasketPage} from '../pages/mm/tabs/basket';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {AlertController} from 'ionic-angular';
import {SafeHtmlPipe} from '../pipes/safe-html/safe-html';

import {CacheModule} from 'ionic-cache';
import {HttpModule} from '@angular/http';
import {DigitalBadgePage} from "../pages/digitalbadge/digitalbadge";

//import { Network } from '@ionic-native/network';

@NgModule({
  declarations: [
    MyApp,
    AgendaPage,
    AnkietaPage, AnkietaThanksPage,
    KtoPage,
    WelcomePage,
    NoclegiPage,
    NewsPage,
    TestPage, Page1Page, Page2Page, Page3Page,
    Login,
    TabsPage,
      MmPage, MmKryteriaPage, MmNagrodyPage, MmRankingPage, MmProductInfoPage, MmBasketPage, MmRankingFiltryPage, MmRankingWidokPage, MmRankingSzukajPage,
    ScorePage,
    SafeHtmlPipe,
    DigitalBadgePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {statusbarPadding: false}),
    CacheModule.forRoot(),

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AgendaPage,
    AnkietaPage, AnkietaThanksPage,
    KtoPage,
    WelcomePage,
    NoclegiPage,
      MmPage, MmKryteriaPage, MmNagrodyPage, MmRankingPage, MmProductInfoPage, MmBasketPage, MmRankingFiltryPage, MmRankingWidokPage, MmRankingSzukajPage,
    NewsPage,
    TestPage, Page1Page, Page2Page, Page3Page,
    Login,
    TabsPage,
    ScorePage, DigitalBadgePage
  ],
  providers: [
    StatusBar,
    SplashScreen, AuthService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AlertController,
    MmRankingPage
  ]
})
export class AppModule {
}
