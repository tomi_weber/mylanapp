import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { CacheService } from "ionic-cache";
import { ToastController } from 'ionic-angular';
//import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
//import 'rxjs/operators/share';

let apiUrl = 'http://api-ios.test001.pl/ajax/';
let headers = new Headers(
    {
        'Content-Type' : 'application/json'
    });
let options = new RequestOptions({ headers: headers });

@Injectable()
export class AuthService {

  constructor(public http : Http, private cache: CacheService, private toastCtrl: ToastController) {
    console.log('Hello AuthService Provider');
  }


  postData(cacheKey, credentials, type) {
    return new Promise((resolve, reject) => {

      this.http.post(apiUrl + type, JSON.stringify(credentials))
          .toPromise()
          .then((response) =>
          {
              console.log('API Response : ', response.json());
              this.cache.saveItem(cacheKey, response.json());
              resolve(response.json());
          })
          .catch((error) =>
          {
              if(this.cache.getItem(cacheKey)) {
                  let toast = this.toastCtrl.create({
                      message: 'Pracujesz w trybie offline',
                      duration: 1000
                  });
                  toast.present();
                  resolve(this.cache.getItem(cacheKey));
              } else {
                  console.error('API Error : ', error.status);
                  console.error('API Error : ', JSON.stringify(error));
                  reject(error.json());
              }
          });
    });

  }

}
