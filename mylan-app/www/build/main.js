webpackJsonp([2],{

/***/ 11:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_cache__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { Observable } from 'rxjs/Observable';


//import 'rxjs/operators/share';
var apiUrl = 'http://api-ios.test001.pl/ajax/';
var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]({
    'Content-Type': 'application/json'
});
var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
var AuthService = (function () {
    function AuthService(http, cache, toastCtrl) {
        this.http = http;
        this.cache = cache;
        this.toastCtrl = toastCtrl;
        console.log('Hello AuthService Provider');
    }
    AuthService.prototype.postData = function (cacheKey, credentials, type) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(apiUrl + type, JSON.stringify(credentials))
                .toPromise()
                .then(function (response) {
                console.log('API Response : ', response.json());
                _this.cache.saveItem(cacheKey, response.json());
                resolve(response.json());
            })
                .catch(function (error) {
                if (_this.cache.getItem(cacheKey)) {
                    var toast = _this.toastCtrl.create({
                        message: 'Pracujesz w trybie offline',
                        duration: 1000
                    });
                    toast.present();
                    resolve(_this.cache.getItem(cacheKey));
                }
                else {
                    console.error('API Error : ', error.status);
                    console.error('API Error : ', JSON.stringify(error));
                    reject(error.json());
                }
            });
        });
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"], __WEBPACK_IMPORTED_MODULE_2_ionic_cache__["b" /* CacheService */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["o" /* ToastController */]])
    ], AuthService);
    return AuthService;
}());

//# sourceMappingURL=auth-service.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WelcomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__agenda_agenda__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ankieta_ankieta__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__kto_kto__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__noclegi_noclegi__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__news_news__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__score_score__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__test_test__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__mm_mm__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var WelcomePage = (function () {
    function WelcomePage(toast, viewCtrl, navCtrl, loadingCtrl, events) {
        this.toast = toast;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.events = events;
        this.siteData = { ankietyName: '', ankietyActive: false, limitPunktow: 0 };
        this.events.publish('hideHeader', { isHidden: true });
        if (localStorage.getItem('userData')) {
            var data = JSON.parse(localStorage.getItem('userData'));
            console.log(data);
            if (data != null) {
                //var tags = data.visitor_line.split(',');
                /*window["plugins"].OneSignal
                      .sendTags({district: data.visitor_district, visitor_function: data.visitor_function, visitor_id: data.visitor_id});

                  for(var i=0;i<tags.length;i++) {
                      window["plugins"].OneSignal.sendTag("line" + i, tags[i]);
                  }*/
            }
        }
        if (localStorage.getItem('siteData')) {
            this.siteData = JSON.parse(localStorage.getItem('siteData'));
        }
    }
    WelcomePage.prototype.ionViewWillEnter = function () {
        this.events.publish('root:created', Date.now());
    };
    WelcomePage.prototype.presentLoadingDefault = function () {
        var loading = this.loadingCtrl.create({
            content: 'Proszę czekać...'
        });
        loading.present();
        setTimeout(function () {
            loading.dismiss();
        }, 1000);
    };
    WelcomePage.prototype.goToNav = function (action) {
        if (action == 'tabAgenda') {
            this.presentLoadingDefault();
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__agenda_agenda__["a" /* AgendaPage */]);
            this.viewCtrl.dismiss();
            this.events.publish('setSiteTitle', { title: 'Agenda' });
            this.events.publish('hideHeader', { isHidden: false });
        }
        if (action == 'tabAnkieta') {
            this.presentLoadingDefault();
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__ankieta_ankieta__["a" /* AnkietaPage */]);
            this.viewCtrl.dismiss();
            this.events.publish('setSiteTitle', { title: 'Ankiety' });
            this.events.publish('hideHeader', { isHidden: false });
        }
        if (action == 'tabKto') {
            this.presentLoadingDefault();
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__kto_kto__["a" /* KtoPage */]);
            this.viewCtrl.dismiss();
            this.events.publish('setSiteTitle', { title: 'Kto jest kto' });
            this.events.publish('hideHeader', { isHidden: false });
        }
        if (action == 'tabNoclegi') {
            this.presentLoadingDefault();
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__noclegi_noclegi__["a" /* NoclegiPage */]);
            this.viewCtrl.dismiss();
            this.events.publish('setSiteTitle', { title: 'Noclegi' });
            this.events.publish('hideHeader', { isHidden: false });
        }
        if (action == 'tabNewsy') {
            this.presentLoadingDefault();
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__news_news__["a" /* NewsPage */]);
            this.viewCtrl.dismiss();
            this.events.publish('setSiteTitle', { title: 'Aktualności' });
            this.events.publish('hideHeader', { isHidden: false });
        }
        if (action == 'tabCele') {
            this.presentLoadingDefault();
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__score_score__["a" /* ScorePage */]);
            this.viewCtrl.dismiss();
            this.events.publish('setSiteTitle', { title: 'Cele' });
            this.events.publish('hideHeader', { isHidden: false });
        }
        if (action == 'tabMm') {
            this.presentLoadingDefault();
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__mm_mm__["a" /* MmPage */]);
            this.viewCtrl.dismiss();
            this.events.publish('setSiteTitle', { title: 'm&m' });
            this.events.publish('hideHeader', { isHidden: true });
        }
        if (action == 'tabTest') {
            this.presentLoadingDefault();
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__test_test__["a" /* TestPage */]);
            this.viewCtrl.dismiss();
            this.events.publish('setSiteTitle', { title: this.siteData.ankietyName });
            this.events.publish('hideHeader', { isHidden: false });
        }
    };
    WelcomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-welcome',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\welcome\welcome.html"*/'<ion-content id="welcome" no-bounce scrollbar-y="false">\n\n    <div class="b-mainWrapper">\n\n        <div class="a-logo">\n\n            <img src="common/images/logo/logo.png" alt="Mylan">\n\n        </div>\n\n        <div class="b-main">\n\n            <h1 class="a-typo is-primary">\n\n                Mylan<br>Navigator\n\n            </h1>\n\n            <nav class="c-menu" style="bottom: 120px">\n\n                <a (click)="goToNav(\'tabAgenda\')" class="c-menu_item">Agenda</a>\n\n                <a (click)="goToNav(\'tabAnkieta\')" class="c-menu_item" *ngIf="this.siteData.ankietyActive">Ankiety</a>\n\n                <a (click)="goToNav(\'tabTest\')" class="c-menu_item iphone5" *ngIf="!this.siteData.ankietyActive">{{ this.siteData.ankietyName }}</a>\n\n                <a (click)="goToNav(\'tabKto\')" class="c-menu_item">Kto jest <span class="is-br"><br></span>kto</a>\n\n                <a (click)="goToNav(\'tabMm\')" class="c-menu_item">M&M</a>\n\n                <a (click)="goToNav(\'tabNewsy\')" class="c-menu_item">Aktualności</a>\n\n                <a (click)="goToNav(\'tabCele\')" class="c-menu_item">Cele</a>\n\n            </nav>\n\n        </div>\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\welcome\welcome.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]) === "function" && _e || Object])
    ], WelcomePage);
    return WelcomePage;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=welcome.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NoclegiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_jquery__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_jquery__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NoclegiPage = (function () {
    function NoclegiPage(navCtrl, viewCtrl, authService, app, alertCtrl, menu, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.authService = authService;
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.userPostData = { "visitor_id": "" };
        this.invitePostData = { "visitor_id": "", "user_id": "" };
        this.invites = [];
        this.invitorsSelect = [];
        menu.enable(true);
        this.events.publish('hideHeader', { isHidden: false });
        var data = JSON.parse(localStorage.getItem('userData'));
        this.userPostData.visitor_id = data.visitor_id;
        this.invitePostData.visitor_id = data.visitor_id;
        this.authService.postData('invoicesList', this.userPostData, 'visitor/getinvite').then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                _this.invites = JSON.parse(_this.responseData.invites);
                console.log(_this.invites);
                //if(this.invites.accepted_active == 0) {
                _this.authService.postData('invitersList', _this.userPostData, 'visitor/getinvitorslist').then(function (result) {
                    _this.responseData = result;
                    if (_this.responseData.status == 'ok') {
                        _this.invitorsSelect = JSON.parse(_this.responseData.invitorsSelect);
                    }
                }, function (err) {
                    console.log('Error' + err);
                });
                //}
            }
        }, function (err) {
            console.log('Error' + err);
        });
    }
    NoclegiPage_1 = NoclegiPage;
    NoclegiPage.prototype.accept = function (request_id) {
        var _this = this;
        this.authService.postData('inviteAccept', this.userPostData, 'visitor/response-invite/accept/' + request_id).then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                _this.navCtrl.push(NoclegiPage_1);
                _this.viewCtrl.dismiss();
            }
        }, function (err) {
            console.log('Error' + err);
        });
    };
    NoclegiPage.prototype.decline = function (request_id) {
        var _this = this;
        this.authService.postData('inviteDecline', this.userPostData, 'visitor/response-invite/decline/' + request_id).then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                _this.navCtrl.push(NoclegiPage_1);
                _this.viewCtrl.dismiss();
            }
        }, function (err) {
            console.log('Error' + err);
        });
    };
    NoclegiPage.prototype.results = function (poll_id) {
        __WEBPACK_IMPORTED_MODULE_3_jquery__('#results_' + poll_id).slideToggle();
    };
    NoclegiPage.prototype.sendRequest = function () {
        var _this = this;
        this.authService.postData('sendRequest', this.invitePostData, 'visitor/addinvite').then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                _this.presentAlert();
                _this.navCtrl.push(NoclegiPage_1);
                _this.viewCtrl.dismiss();
            }
            else {
                alert('Wystąpił błąd podczas wysyłania zaproszenia.');
            }
        }, function (err) {
            console.log('Error' + err);
        });
    };
    NoclegiPage.prototype.presentAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Twoje zaproszenie zostało wysłane',
            subTitle: '',
            buttons: ['Zamknij']
        });
        alert.present();
    };
    NoclegiPage.prototype.presentAlert2 = function () {
        var alert = this.alertCtrl.create({
            title: 'Twoje zaproszenie zostało wysłane',
            subTitle: '',
            buttons: ['Zamknij']
        });
        alert.present();
    };
    NoclegiPage = NoclegiPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-noclegi',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\noclegi\noclegi.html"*/'<ion-content no-bounce scrollbar-y="false">\n\n<div class="b-mainWrapper is-category">\n\n    <div class="b-main c-accordion">\n\n        <div class="c-accordion_item js-accordion_item is-active" *ngIf="invites.accepted_active > 0">\n\n            <h2 class="c-accordion_title js-accordion_title">Gratulacje!</h2>\n\n            <div *ngFor="let item of invites.accepted;" class="c-accordion_item is-active">\n\n                <div class="c-form_row c-accordion_content">\n\n                    <p class="c-form_headline" style="color: #48b14c; line-height: 24px; margin-bottom: 10px;">\n\n                       Zostałeś/aś poprawnie sparowany/a.</p>\n\n                    <span>{{ item.visitor_firstname_inviter }} {{ item.visitor_lastname_inviter }} - {{ item.visitor_firstname }} {{ item.visitor_lastname }}</span>\n\n                </div>\n\n            </div>\n\n        </div>\n\n        <div class="c-accordion_item js-accordion_item is-active" *ngIf="invites.recived_active > 0">\n\n            <h2 class="c-accordion_title js-accordion_title">Zaproszenia oczekujące na akceptację</h2>\n\n            <article *ngFor="let item of invites.recived;" class="c-accordion_content">\n\n                <div class="c-accordion_bio" style="margin-bottom: 10px; padding-left: 0;">\n\n                    <p class="c-accordion_name">Od: {{ item.visitor_firstname_inviter }} {{ item.visitor_lastname_inviter }}</p>\n\n                    <p class="c-accordion_description">Otrzymane dnia: {{ item.invite_date }}</p><br />\n\n                    <div class="c-form_row">\n\n                        <div>\n\n                            <button (tap)="accept(item.visitor_invite_id)" class="c-form_btn"\n\n                                    style="margin-right: 25px; font-size: 14px; color: #fff; margin-top: 5px; height: 30px; line-height: 20px; min-width: 100px; border-radius: 16px; background-color: limegreen;">\n\n                                Zaakceptuj\n\n                            </button>\n\n\n\n                            <button (tap)="decline(item.visitor_invite_id)" class="c-form_btn"\n\n                                    style="font-size: 14px; color: #fff; margin-top: 5px; height: 30px; line-height: 20px; min-width: 100px; border-radius: 16px; background-color: red;">\n\n                                Odrzuć\n\n                            </button>\n\n                        </div>\n\n                    </div>\n\n                </div>\n\n            </article>\n\n        </div>\n\n        <div class="c-accordion_item js-accordion_item is-active" *ngIf="invites.sended_active > 0">\n\n            <h2 class="c-accordion_title js-accordion_title">Wysłane zaproszenia</h2>\n\n            <article *ngFor="let item of invites.sended;" class="c-accordion_content">\n\n                <div class="c-accordion_bio" style="margin-bottom: 10px; padding-left: 0;">\n\n                    <p class="c-accordion_name">Do: {{ item.visitor_firstname }} {{ item.visitor_lastname }}</p>\n\n                    <p class="c-accordion_description">Wysłane dnia: {{ item.invite_date }}</p>\n\n                    <br />\n\n                    <p class="c-accordion_email" *ngIf="item.invite_declined == 1">\n\n                        <span style="color: orangered; font-size: 1.0rem">Twoje zaproszenie zostało odrzucone.</span>\n\n                    </p>\n\n                    <p class="c-accordion_email" *ngIf="item.invite_declined == 0">\n\n                        <span style="color: darkslategray; font-size: 1.0rem">Zaproszenie oczekuje na akceptację.</span>\n\n                    </p>\n\n                </div>\n\n            </article>\n\n        </div>\n\n        <div class="c-accordion_item js-accordion_item is-active" *ngIf="invites.recived_declined_active > 0">\n\n            <h2 class="c-accordion_title js-accordion_title">Otrzymane zaproszenia</h2>\n\n            <article *ngFor="let item of invites.recived_declined;" class="c-accordion_content">\n\n                <div class="c-accordion_bio" style="margin-bottom: 10px; padding-left: 0;">\n\n                    <p class="c-accordion_name">Od: {{ item.visitor_firstname_inviter }} {{ item.visitor_lastname_inviter }}</p>\n\n                    <p class="c-accordion_description">Otrzymane dnia: {{ item.invite_date }}</p>\n\n                    <br />\n\n                    <p class="c-accordion_email" *ngIf="item.invite_declined == 1">\n\n                        <span style="color: orangered; font-size: 1.0rem">Twoje zaproszenie zostało odrzucone.</span>\n\n                    </p>\n\n                    <p class="c-accordion_email" *ngIf="item.invite_declined == 0">\n\n                        <span style="color: darkslategray; font-size: 1.0rem">Zaproszenie oczekuje na akceptację.</span>\n\n                    </p>\n\n                </div>\n\n            </article>\n\n        </div>\n\n\n\n        <div class="c-form" *ngIf="invites.accepted_active == 0">\n\n\n\n\n\n            <div class="c-accordion_item js-accordion_item is-active" *ngIf="invites.new_blocked == 0">\n\n                <h2 class="c-accordion_title js-accordion_title">Wyślij zaproszenie</h2>\n\n                <form action="" id="js-login">\n\n                    <div class="c-form_row">\n\n                        <div class="c-form_input">\n\n\n\n                                <ion-select style="max-width: 95%; margin: 0 auto; font-size: 16px; padding: 0 20px; width: 100%; border: 1px solid #D7D7D7; border-radius: 22px; height: 45px; line-height: 45px;" [(ngModel)]="invitePostData.user_id"\n\n                                            [ngModelOptions]="{standalone: true}">\n\n                                    <ion-option value="" style="font-size: 16px">Wybierz osobę</ion-option>\n\n                                    <ion-option style="font-size: 16px" *ngFor="let item of invitorsSelect.users;"\n\n                                                value="{{ item.visitor_id }}">{{ item.visitor_firstname }} {{ item.visitor_lastname }}\n\n                                    </ion-option>\n\n                                </ion-select>\n\n\n\n                        </div>\n\n                    </div>\n\n                    <div class="c-form_row is-submit">\n\n                        <button tappable (tap)="sendRequest()" class="c-form_btn">Wyślij</button>\n\n                    </div>\n\n                </form>\n\n            </div>\n\n        </div>\n\n    </div>\n\n</div>\n\n</ion-content>'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\noclegi\noclegi.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], NoclegiPage);
    return NoclegiPage;
    var NoclegiPage_1;
}());

//# sourceMappingURL=noclegi.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NewsPage = (function () {
    function NewsPage(navCtrl, viewCtrl, authService, navParams, app, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.authService = authService;
        this.navParams = navParams;
        this.app = app;
        this.events = events;
        this.newsList = [];
        this.newsId = 0;
        this.toogled = false;
        this.userPostData = { "visitor_id": "" };
        this.events.publish('hideHeader', { isHidden: false });
        var data = JSON.parse(localStorage.getItem('userData'));
        this.userPostData.visitor_id = data.visitor_id;
        this.authService.postData('newsList', this.userPostData, 'news/userlist').then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                _this.newsList = JSON.parse(_this.responseData.userData);
                if (navParams.get('param_news_id') != null) {
                    _this.newsId = navParams.get('param_news_id');
                }
            }
        }, function (err) {
        });
    }
    NewsPage.prototype.showAgenda = function (date) {
        var parentElement = '.js-accordion_item';
        var activeClass = 'is-active';
        var container = '#js-accordion';
        var $this = $('#js-accordion_title2_' + date);
        if ($this.parent(parentElement).hasClass(activeClass)) {
            $this.parents(container).find(parentElement).removeClass(activeClass);
            $this.parent(parentElement).removeClass(activeClass);
        }
        else {
            $this.parents(container).find(parentElement).removeClass(activeClass);
            $this.parent(parentElement).addClass(activeClass);
        }
    };
    NewsPage.prototype.toggleBox2 = function (id) {
        $('#box_full_' + id).toggle();
        $('#box_short_' + id).toggle();
        $('#click_full_' + id).parent().parent().parent().toggleClass('is-active');
    };
    NewsPage.prototype.executeAfterPush = function (lastItem) {
        var _this = this;
        console.log('LAST item' + lastItem);
        if (lastItem && this.newsId != 0 && !this.toogled) {
            setTimeout(function () {
                _this.toggleBox2(_this.newsId);
            }, 1000);
            this.toogled = true;
        }
    };
    NewsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-news',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\news\news.html"*/'<ion-content no-bounce scrollbar-y="false">\n\n<div class="b-mainWrapper is-category">\n\n  <div class="b-main">\n\n    <div class="c-accordion" id="js-accordion" style="margin-bottom: 50px; margin-right: 5px;">\n\n      <div class="c-accordion_item js-accordion_item is-active">\n\n        <div *ngFor="let item of newsList;  let last = last;" item-content  class="c-accordion_content">\n\n          {{executeAfterPush(last)}}\n\n          <article>\n\n            <header class="c-accordion_header">\n\n                      <span class="c-accordion_data">\n\n                        {{ item.news_date_add }}\n\n                      </span>\n\n\n\n            </header>\n\n            <article class="c-accordion_text">\n\n              <h3 style="margin-top: 1.0rem">{{ item.news_title }}</h3>\n\n              <p *ngIf="item.news_desc != \'\'" id="box_short_{{ item.news_id }}" [innerHTML]="item.news_desc | safeHtml">{{ item.news_desc }}</p>\n\n              <p *ngIf="item.news_desc_full != \'\'" style="display: none" id="box_full_{{ item.news_id }}"><img *ngIf="item.news_photo != null" class="img-responsive" src="http://api-ios.test001.pl/data/news_images/{{ item.news_photo }}" style="padding: 5px 0"> <span [innerHTML]="item.news_desc_full | safeHtml">{{ item.news_desc_full }}</span></p>\n\n            </article>\n\n            <footer class="c-accordion_footer">\n\n            </footer>\n\n            <span *ngIf="item.news_desc_full != \'\'" class="c-accordion_add" style="cursor: pointer;"><i (click)="toggleBox2(item.news_id)" class="icon-more01 icon-close01" id="click_full_{{ item.news_id }}"></i></span>\n\n          </article>\n\n        </div>\n\n        <div *ngIf=\'newsList?.length === 0\' item-content  class="c-accordion_content" >Zakładka nie zawiera w tej chwili żadnych informacji. </div>\n\n      </div>\n\n    </div>\n\n  </div>\n\n</div>\n\n</ion-content>'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\news\news.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], NewsPage);
    return NewsPage;
}());

//# sourceMappingURL=news.js.map

/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScorePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ScorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ScorePage = (function () {
    function ScorePage(navCtrl, viewCtrl, authService, navParams, app, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.authService = authService;
        this.navParams = navParams;
        this.app = app;
        this.events = events;
        this.scoreList = [];
        this.userPostData = { "visitor_id": "" };
        this.events.publish('hideHeader', { isHidden: false });
        var data = JSON.parse(localStorage.getItem('userData'));
        this.userPostData.visitor_id = data.visitor_id;
        this.authService.postData('scoreList', this.userPostData, 'news/score/userlist').then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                _this.scoreList = JSON.parse(_this.responseData.userData);
            }
        }, function (err) {
        });
    }
    ScorePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ScorePage');
    };
    ScorePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-score',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\score\score.html"*/'<ion-content no-bounce scrollbar-y="false">\n\n  <div class="b-mainWrapper is-category">\n\n    <div class="b-main">\n\n      <div class="c-accordion" id="js-accordion">\n\n        <div class="c-accordion_item js-accordion_item is-active">\n\n          <div *ngFor="let item of scoreList;  let last = last;" item-content  class="c-accordion_content">\n\n              <article class="c-accordion_text">\n\n                <div [innerHTML]="item.news_desc | safeHtml">\n\n                  {{ item.news_desc }}\n\n                </div>\n\n              </article>\n\n          </div>\n\n          <div *ngIf=\'scoreList?.length === 0\' item-content  class="c-accordion_content" >Zakładka nie zawiera w tej chwili żadnych informacji. </div>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n\n  <style type="text/css">\n\n    td, th {\n\n      padding: 0;\n\n      padding: 5px 10px;\n\n      background: #44c1de;\n\n      margin: 5px;\n\n      border: 1px solid #409fb5;\n\n    }\n\n  </style>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\score\score.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], ScorePage);
    return ScorePage;
}());

//# sourceMappingURL=score.js.map

/***/ }),

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_jquery__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_jquery__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TestPage = (function () {
    function TestPage(navCtrl, viewCtrl, authService, app, alertCtrl, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.authService = authService;
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.userPostData = { "visitor_id": "" };
        this.start = false;
        this.startTime = 0;
        this.votes = [];
        this.ranking = [];
        this.tab = [];
        this.events.publish('hideHeader', { isHidden: false });
        var data = JSON.parse(localStorage.getItem('userData'));
        this.userPostData.visitor_id = data.visitor_id;
        this.tab.push({ active: false });
        this.tab.push({ active: false });
        this.tab.push({ active: false });
        this.authService.postData('ankietaList', this.userPostData, 'polltest/get').then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                _this.ankiety = JSON.parse(_this.responseData.polls);
                _this.txt = JSON.parse(_this.responseData.txt);
                _this.txt2 = JSON.parse(_this.responseData.txt2);
                _this.authService.postData('rankingListUser', _this.userPostData, 'polltest/rank/' + _this.userPostData.visitor_id).then(function (result) {
                    _this.responseData = result;
                    //if(this.responseData.status == 'ok') {
                    _this.rankingUser = JSON.parse(_this.responseData.aswer);
                    _this.authService.postData('rankingList', _this.userPostData, 'polltest/rank').then(function (result) {
                        _this.responseData = result;
                        if (_this.responseData.status == 'ok') {
                            _this.ranking = JSON.parse(_this.responseData.answer);
                            console.log(_this.ranking);
                            var ii = 1;
                            for (var item in _this.ranking) {
                                if (_this.ranking[item].user_id == parseInt(_this.userPostData.visitor_id)) {
                                    _this.rankingUser.miejsce = ii;
                                }
                                ii++;
                            }
                            if (!_this.ankiety || _this.ankiety == 'false' || _this.ankiety == false) {
                                _this.tab[2].active = true;
                            }
                        }
                    }, function (err) {
                        console.log('Error' + err);
                    });
                    //}
                }, function (err) {
                    console.log('Error' + err);
                });
                for (var poll in _this.ankiety) {
                    _this.votes.push(0);
                }
                console.log(_this.ankiety);
                console.log(_this.votes);
            }
        }, function (err) {
            console.log('Error' + err);
        });
    }
    TestPage.prototype.showAgenda = function (date) {
        var parentElement = '.js-accordion_item';
        var activeClass = 'is-active';
        var container = '#js-accordion';
        var $this = __WEBPACK_IMPORTED_MODULE_3_jquery__('#js-accordion_title2_' + date);
        setTimeout(function () {
            if ($this.parent(parentElement).hasClass(activeClass)) {
                $this.parents(container).find(parentElement).removeClass(activeClass);
                $this.parent(parentElement).removeClass(activeClass);
            }
            else {
                $this.parents(container).find(parentElement).removeClass(activeClass);
                $this.parent(parentElement).addClass(activeClass);
            }
        }, 300);
    };
    TestPage.prototype.startVote = function () {
        this.start = true;
        this.startTime = Date.now();
        console.log(this.startTime);
    };
    TestPage.prototype.vote = function () {
        var _this = this;
        console.log(Date.now());
        var time = Date.now() - this.startTime;
        this.authService.postData('ankietaVote', this.votes, 'polltest/vote/' + this.userPostData.visitor_id + '/' + time).then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                _this.authService.postData('rankingListUser', _this.userPostData, 'polltest/rank/' + _this.userPostData.visitor_id).then(function (result) {
                    _this.responseData = result;
                    if (_this.responseData.status == 'ok') {
                        _this.rankingUser = JSON.parse(_this.responseData.aswer);
                        _this.authService.postData('rankingList', _this.userPostData, 'polltest/rank').then(function (result) {
                            _this.responseData = result;
                            if (_this.responseData.status == 'ok') {
                                _this.ranking = JSON.parse(_this.responseData.answer);
                                var ii = 1;
                                for (var item in _this.ranking) {
                                    if (_this.ranking[item].user_id == parseInt(_this.userPostData.visitor_id)) {
                                        _this.rankingUser.miejsce = ii;
                                    }
                                    ii++;
                                }
                                _this.tab[2].active = true;
                                _this.tab[0].active = false;
                                _this.tab[1].active = false;
                                _this.presentAlert();
                                _this.ankiety = false;
                            }
                        }, function (err) {
                            console.log('Error' + err);
                        });
                    }
                }, function (err) {
                    console.log('Error' + err);
                });
            }
            else {
                alert('Wystąpił błąd podczas oddawania głosu.');
            }
        }, function (err) {
            console.log('Error' + err);
        });
    };
    TestPage.prototype.toggleSection = function (i) {
        if (!this.tab[i].active) {
            this.tab[0].active = false;
            this.tab[1].active = false;
            this.tab[2].active = false;
        }
        this.tab[i].active = !this.tab[i].active;
    };
    TestPage.prototype.presentAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Dziękujemy za oddanie głosu',
            subTitle: 'Liczba Twoich poprawnych odpowiedzi: ' + this.rankingUser.odpowiedzi +
                '<br>' +
                'Twój czas: ' + this.rankingUser.czas +
                '<br>' +
                'Aktualnie zajmujesz ' + this.rankingUser.miejsce + ' miejsce',
            buttons: ['Zamknij']
        });
        alert.present();
    };
    TestPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-test',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\test\test.html"*/'<ion-content no-bounce scrollbar-y="false">\n\n    <div class="b-mainWrapper is-category">\n\n        <div class="b-main">\n\n            <div class="c-form c-accordion">\n\n                <!-- First Level -->\n\n                <div no-lines no-padding [ngClass]="{\'is-active c-accordion_item\': tab[0].active, \'c-accordion_item\': !tab[0].active}">\n\n                    <h2 class="c-accordion_title js-accordion_title" id="js-accordion_title2_1" (click)="toggleSection(0)">\n\n                        Zasady\n\n                        <i class="icon-arrow02"></i>\n\n                    </h2>\n\n\n\n                    <article class="c-accordion_content">\n\n                        <div [innerHTML]="txt | safeHtml">\n\n                            {{ txt }}<br /><br />\n\n                        </div>\n\n                        <div [innerHTML]="txt2| safeHtml">\n\n                            {{ txt2 }}\n\n                        </div>\n\n                    </article>\n\n                </div>\n\n\n\n                <div no-lines no-padding [ngClass]="{\'is-active c-accordion_item\': tab[1].active, \'c-accordion_item\': !tab[1].active}">\n\n                    <h2 class="c-accordion_title js-accordion_title" id="js-accordion_title2_2"\n\n                        tappable (tap)="toggleSection(1)">Odpowiadaj\n\n                        <i class="icon-arrow02"></i>\n\n                    </h2>\n\n                    <article class="c-accordion_content" *ngIf="!ankiety">\n\n                        <div class="c-form_row">\n\n                            <p class="c-form_headline">Odpowiedziałeś już na wszystkie pytania, które przygotowaliśmy. Zobacz jak poradziłeś sobie na tle innych w zakładce Ranking.</p>\n\n                        </div>\n\n                    </article>\n\n                    <article class="c-accordion_content" *ngIf="ankiety && !start" style="background: none">\n\n                        <div class="c-form" style="text-align: center">\n\n                            <button (click)="startVote()" class="c-form_btn">START</button>\n\n                        </div>\n\n                    </article>\n\n                    <article class="c-accordion_content" *ngIf="ankiety && start">\n\n                        <div class="c-form" *ngFor="let item of ankiety; let i = index" style="margin-bottom: 30px;">\n\n                            <div class="c-form_row">\n\n                                <p class="c-form_headline" style="margin-bottom: 5px; font-weight: bold">{{ item.question[0].question_title }}</p>\n\n                                <img *ngIf="item.question[0].image" src="http://api-ios.test001.pl/data/ankieta/{{ item.polling_id }}.jpg" class="" />\n\n                                <video *ngIf="item.question[0].video" style="width: 100%; height: 100%;" class="video-player" controls>\n\n                                    <source src="http://api-ios.test001.pl/data/video/{{ item.polling_id }}.mp4" type="video/mp4" />\n\n                                </video>\n\n                            </div>\n\n                            <div class="c-form_row" *ngFor="let item_answer of item.answer">\n\n                                <label class="c-form_radio">\n\n                                    <input type="radio" [name]="\'radio\' + i" [(ngModel)]="votes[i]"\n\n                                           [ngModelOptions]="{standalone: true}" [value]="item_answer.polling_answer_id">\n\n                                    <span class="{{ item_answer.answer_letter }}"><i>{{ item_answer.answer_letter }}</i>\n\n                                        <div style="height: 55px; display: inline-block; width: 80%" class="iphoneAnswerParent">\n\n                                            <div style="display: table-cell; height: 55px; vertical-align: middle;" class="iphoneAnswer">\n\n                                                {{ item_answer.answer_title }}\n\n                                            </div>\n\n                                        </div>\n\n                                    </span>\n\n                                </label>\n\n                            </div>\n\n                        </div>\n\n                        <div class="c-form c-form_row is-submit" style="text-align: center;">\n\n                            <button (click)="vote()" class="c-form_btn" style="margin-top: 10px; margin-bottom: 100px;">Wyślij</button>\n\n                        </div>\n\n                    </article>\n\n                </div>\n\n\n\n                <div no-lines no-padding [ngClass]="{\'is-active c-accordion_item\': tab[2].active, \'c-accordion_item\': !tab[2].active}">\n\n                    <h2 class="c-accordion_title js-accordion_title" id="js-accordion_title2_3"\n\n                        tappable (tap)="toggleSection(2)">Ranking\n\n                        <i class="icon-arrow02"></i>\n\n                    </h2>\n\n                    <article class="c-accordion_content">\n\n                        <h3 *ngIf="rankingUser" class="c-form_headline" style="margin-bottom: 15px; white-space: normal; letter-spacing: 0px; text-transform: none; font-size: 1rem; font-weight: 700; font-family: Arial">Twoje miejsce: {{rankingUser.miejsce}}<br />Poprawne odpowiedzi: {{rankingUser.odpowiedzi}}<br />Czas: {{rankingUser.czas}}</h3>\n\n                        <table *ngIf="ranking" >\n\n                            <tr *ngFor="let rank of ranking; let i2 = index" style="border-bottom: 1px solid #d4d4d4;">\n\n                                <td style="background: #f1f1f1;padding: 2px 5px;">{{i2+1}}</td>\n\n                                <td style="background: #f1f1f1;padding: 2px 5px;">{{rank.user}}</td>\n\n                                <td style="background: #f1f1f1;padding: 2px 5px;">{{rank.correct}}/{{rank.all}}</td>\n\n                                <td style="background: #f1f1f1;padding: 2px 5px;">{{rank.time_format}}</td>\n\n                            </tr>\n\n                        </table>\n\n                    </article>\n\n                </div>\n\n            </div>\n\n        </div>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\test\test.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], TestPage);
    return TestPage;
}());

//# sourceMappingURL=test.js.map

/***/ }),

/***/ 126:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 126;

/***/ }),

/***/ 169:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/mm/mm.module": [
		304,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 169;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return MmRankingPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MmRankingFiltryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__mm__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__nagrody__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MmRankingPage = (function () {
    function MmRankingPage(viewCtrl, navCtrl, authService, app, events, popoverCtrl) {
        var _this = this;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.app = app;
        this.events = events;
        this.popoverCtrl = popoverCtrl;
        //m123451
        this.siteData = { ankietyName: '', ankietyActive: false, limitPunktow: 0 };
        this.userData = { visitor_id: '', points: 0, ranking: 0, rankingPoints: 0, visitor_number: '', visitor_line: '', username: '' };
        this.rankingRep = [];
        this.rankingAsm = [];
        this.rankingLine = [];
        this.rankingToView = [];
        this.asm = false;
        this.su = false;
        this.asmList = [];
        this.selectedAsm = '';
        this.selectedFilter = 0;
        var data = JSON.parse(localStorage.getItem('userData'));
        this.userData.visitor_id = data.visitor_id;
        this.userData.points = data.visitor_points;
        this.userData.visitor_number = data.visitor_number;
        this.userData.username = data.username;
        this.userData.visitor_line = data.visitor_line;
        if (localStorage.getItem('siteData')) {
            this.siteData = JSON.parse(localStorage.getItem('siteData'));
        }
        this.authService.postData('rankingList', this.userData, 'mm/ranking').then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                _this.rankingRep = JSON.parse(_this.responseData.rankingRep);
                _this.rankingAsm = JSON.parse(_this.responseData.rankingAsm);
                _this.rankingLine = JSON.parse(_this.responseData.rankingLine);
            }
            for (var _i = 0, _a = _this.responseData.rankingRep; _i < _a.length; _i++) {
                var item = _a[_i];
                if (item.mm_user_number == _this.userData.visitor_number) {
                    _this.userData.ranking = item.mm_points;
                    _this.userData.rankingPoints = item.mm_position;
                }
            }
            _this.rankingToView = Object(__WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"])(_this.rankingRep);
        }, function (err) {
        });
        if (this.userData.visitor_line.indexOf('ASM') > -1) {
            this.asm = true;
            this.selectedFilter = 1;
            this.selectedAsm = this.userData.visitor_number;
        }
        else if (this.userData.visitor_line.indexOf('SU') > -1) {
            this.su = true;
        }
        events.subscribe('selectRankingType', function (type) {
            _this.selectedFilter = type;
            _this.showRanking();
        });
        events.subscribe('selectAsm', function (asm) {
            _this.selectedAsm = asm;
            _this.showRanking();
        });
    }
    MmRankingPage.prototype.showRanking = function () {
        var _this = this;
        if (this.selectedFilter == 2) {
            this.rankingToView = Object(__WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"])(this.rankingLine);
        }
        if (this.selectedFilter == 0) {
            this.rankingToView = Object(__WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"])(this.rankingRep);
        }
        if (this.selectedFilter == 1) {
            if (this.su) {
                this.rankingToView = Object(__WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"])(this.rankingAsm);
            }
            else {
                this.rankingToView = Object(__WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"])(this.rankingRep);
                this.rankingToView = this.rankingToView.filter(function (item) { return item.mm_asm_number == _this.selectedAsm; });
            }
        }
    };
    MmRankingPage.prototype.backToMm = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__mm__["a" /* MmPage */]);
        this.viewCtrl.dismiss();
    };
    MmRankingPage.prototype.goToNagrody = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__nagrody__["a" /* MmNagrodyPage */]);
        this.viewCtrl.dismiss();
    };
    MmRankingPage.prototype.filtruj = function () {
        var popover = this.popoverCtrl.create(MmRankingFiltryPage, {
            asm: this.asm,
            su: this.su,
            user: this.userData,
            selectedFilter: this.selectedFilter,
            selectedAsm: this.selectedAsm,
            asmList: this.rankingAsm
        });
        popover.present();
    };
    MmRankingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mm-ranking',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\mm\tabs\ranking.html"*/'<ion-content no-bounce scrollbar-y="false">\n\n    <div class="b-mainWrapper is-rank">\n\n        <div class="b-main">\n\n            <header class="b-header">\n\n                <a (click)="backToMm()" class="is-backBtn"><i class="icon-arrow02"></i> Miles & More</a>\n\n                <label for="menu"><i class="icon-menu01"></i></label>\n\n            </header>\n\n            <div class="b-user">\n\n                <div class="b-user_pic">\n\n                    <span class="is-count">{{ userData.ranking }}</span>\n\n                    <div class="b-user_mask">\n\n                        <img src="https://via.placeholder.com/150" alt="">\n\n                    </div>\n\n                </div>\n\n                <p class="a-typo is-text">{{ userData.username }}</p>\n\n                <p class="a-typo is-primary" style="position: static">{{ userData.rankingPoints }} pkt.</p>\n\n                <p *ngIf="siteData.limitPunktow > userData.points" class="a-typo is-text is-dark">Do wymiany pkt na nagrody brakuje: <strong>{{ siteData.limitPunktow-userData.points }} pkt.</strong></p>\n\n                <p *ngIf="siteData.limitPunktow <= userData.points" class="a-typo is-text is-dark">Do wykorzystania pozostało: <strong>{{ userData.points }} pkt.</strong></p>\n\n                <p (click)="goToNagrody()" *ngIf="siteData.limitPunktow <= userData.points" class="a-btn is-primary">Wymień punkty</p>\n\n            </div>\n\n            <div class="b-ranking">\n\n                <div class="b-ranking_headline">\n\n                    <p class="a-typo is-secondary">Ranking</p> <button class="a-btn is-primary" (click)="filtruj()">Filtruj/Sortuj</button>\n\n                </div>\n\n                <div class="b-ranking_list">\n\n                    <ul>\n\n                        <li [ngClass]="\'\' + item.mm_user_number == userData.visitor_number || (asm && userData.visitor_number == item.mm_asm_number) ? \' is-active\' : \'\'" class="b-ranking_listItem" *ngFor="let item of rankingToView">\n\n                            <div class="b-ranking_img">\n\n                                <span class="is-buble">{{ item.mm_position }} </span>\n\n                                <span *ngIf="item.mm_position > item.mm_last_position" class="is-buble is-down">{{ item.mm_position-item.mm_last_position }} &#8595;</span>\n\n                                <span *ngIf="item.mm_position < item.mm_last_position" class="is-buble is-up">{{ item.mm_last_position-item.mm_position }} &#8593;</span>\n\n                                <img src="https://via.placeholder.com/100" alt="">\n\n                            </div>\n\n                            <div class="b-ranking_data" *ngIf="selectedFilter != 2">\n\n                                <p class="a-typo is-text">{{ item.visitor_firstname }} {{ item.visitor_lastname }}</p>\n\n                                <p class="a-typo is-text">ASM: <span>{{ item.asm_lastname }}</span> PKT:<span>{{ item.mm_points }}</span> <span>{{ item.mm_user_line }}</span></p>\n\n                            </div>\n\n                            <div class="b-ranking_data" *ngIf="selectedFilter == 2">\n\n                                <p class="a-typo is-text">{{ item.mm_line }}</p>\n\n                                <p class="a-typo is-text">PKT:<span>{{ item.mm_points }}</span></p>\n\n                            </div>\n\n                        </li>\n\n\n\n                    </ul>\n\n                </div>\n\n            </div>\n\n        </div>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\mm\tabs\ranking.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* PopoverController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* PopoverController */]) === "function" && _f || Object])
    ], MmRankingPage);
    return MmRankingPage;
    var _a, _b, _c, _d, _e, _f;
}());

var MmRankingFiltryPage = (function () {
    function MmRankingFiltryPage(viewCtrl, navParams, events, toastController, alertController) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.events = events;
        this.toastController = toastController;
        this.alertController = alertController;
        this.asm = false;
        this.su = false;
        this.asmList = [];
        this.selectedFilter = 0;
        this.selectedAsm = 0;
        this.showAsmBox = false;
        this.su = this.navParams.get('su');
        this.asm = this.navParams.get('asm');
        this.user = this.navParams.get('user');
        this.asmList = this.navParams.get('asmList');
        this.selectedFilter = this.navParams.get('selectedFilter');
        this.selectedAsm = this.navParams.get('selectedAsm');
        if (this.selectedAsm != 0) {
            this.showAsmBox = true;
        }
    }
    MmRankingFiltryPage.prototype.selectFilter = function (filter) {
        this.selectedFilter = filter;
        if (filter == 1 && !this.su) {
            this.showAsmBox = true;
        }
        else {
            this.events.publish('selectAsm', '');
            this.events.publish('selectRankingType', filter);
            this.showAsmBox = false;
            this.viewCtrl.dismiss();
        }
    };
    MmRankingFiltryPage.prototype.selectAsm = function ($event) {
        this.events.publish('selectRankingType', this.selectedFilter);
        this.events.publish('selectAsm', this.selectedAsm);
        this.viewCtrl.dismiss();
    };
    MmRankingFiltryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mm-ranking-filtry',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\mm\tabs\ranking-filtry.html"*/'<div class="b-precart" style="margin: 0 !important">\n\n    <div class="b-precart_inner" style="padding:25px; border-radius: 0">\n\n        <ul>\n\n            <li (click)="selectFilter(0)">Wszyscy REP</li>\n\n            <li (click)="selectFilter(1)">ASM</li>\n\n            <li (click)="selectFilter(2)">Linie</li>\n\n        </ul>\n\n        <div style="clear: both;"></div>\n\n        <div *ngIf="showAsmBox && !su">\n\n            <ion-select name="asm" okText="Wybierz" cancelText="Anuluj" required id="asm" (ionChange)="selectAsm($event);" [(ngModel)]="selectedAsm" [ngModelOptions]="{standalone: true}">\n\n                <ion-option *ngFor="let option of asmList; let k= index;" [value]="option.mm_user_number">{{ option.visitor_lastname }}</ion-option>\n\n            </ion-select>\n\n        </div>\n\n    </div>\n\n</div>\n\n'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\mm\tabs\ranking-filtry.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _e || Object])
    ], MmRankingFiltryPage);
    return MmRankingFiltryPage;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=ranking.js.map

/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MmProductInfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MmProductInfoPage = (function () {
    function MmProductInfoPage(viewCtrl, navParams, toastController, alertController) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.toastController = toastController;
        this.alertController = alertController;
        this.amount = 1;
        this.siteData = { ankietyName: '', ankietyActive: false, limitPunktow: 0 };
        this.product = this.navParams.get('product');
        this.user = this.navParams.get('user');
        if (localStorage.getItem('siteData')) {
            this.siteData = JSON.parse(localStorage.getItem('siteData'));
        }
    }
    MmProductInfoPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    MmProductInfoPage.prototype.addToOrder = function (product) {
        var itemsSum = 0;
        var items = [];
        if (localStorage.getItem('basketData')) {
            var data = JSON.parse(localStorage.getItem('basketData'));
            if (data != null) {
                itemsSum = data.itemSum;
                items = data.items;
            }
        }
        var ordersSumPrice = 0;
        if (items.length > 0) {
            for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
                var item = items_1[_i];
                ordersSumPrice += item.product.product_points * item.amount;
            }
        }
        console.log('ordersSumPrice', ordersSumPrice);
        if ((ordersSumPrice + (this.amount * product.product_points)) <= this.user.points && this.user.points > 0) {
            itemsSum += this.amount;
            items.push({ product: product, amount: this.amount, id: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) });
            localStorage.setItem('basketData', JSON.stringify({
                itemSum: itemsSum,
                items: items
            }));
            var toast = this.toastController.create({
                message: 'Produkt został dodany do koszyka',
                duration: 2000
            });
            toast.present();
        }
        else {
            var alert_1 = this.alertController.create({
                title: 'Uwaga',
                subTitle: 'Produkt nie został dodany do koszuka',
                message: 'Wartość punktowa zamawianych produktów przekroczyła ilość możliwych do użycia punktów.',
                buttons: ['OK']
            });
            alert_1.present();
        }
        this.viewCtrl.dismiss();
    };
    MmProductInfoPage.prototype.removeAmount = function () {
        this.amount--;
        if (this.amount < 1) {
            this.amount = 1;
        }
    };
    MmProductInfoPage.prototype.addAmount = function () {
        this.amount++;
    };
    MmProductInfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mm-nagrody-info',
            template: "\n        <div class=\"b-main is-precart\" style=\"text-align: center; background: inherit\">\n            <a (click)=\"close()\" class=\"a-btn is-primary\">\n                Zamknij\n            </a>\n                <div class=\"b-precart\">\n                    <div class=\"b-precart_img\">\n                        <span class=\"is-info\">{{ product.product_title }}<span>{{ product.product_points }} pkt.</span></span>\n                        <img src=\"http://api-ios.test001.pl/data/product_img/{{ product.mm_product_id }}.jpg\" alt=\"\">\n                    </div>\n                    <div class=\"b-precart_inner\">\n                        <div class=\"b-precart_data\">\n                            {{ product.category_name }}\n                        </div>\n                        <div class=\"b-precart_desc\">\n                            {{ product.product_opis }}\n                            \n                            <div class=\"b-precart_footer\">\n                                <div class=\"is-changeCount\">\n                                    <span class=\"is-plus\" (click)=\"addAmount()\">+</span>\n                                    <span class=\"is-count\">{{ amount }}</span>\n                                    <span class=\"is-minus\" (click)=\"removeAmount()\">-</span>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <a *ngIf=\"this.user.points >= this.siteData.limitPunktow\" (click)=\"addToOrder(product)\" class=\"a-btn is-primary\">\n                    Dodaj do zam\u00F3wienia\n                </a>\n                <p *ngIf=\"this.user.points < this.siteData.limitPunktow\"  class=\"a-typo is-text is-dark\">Do wymiany pkt na nagrody brakuje: <strong>{{ this.siteData.limitPunktow-this.user.points }} pkt.</strong></p>\n        </div>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], MmProductInfoPage);
    return MmProductInfoPage;
}());

//# sourceMappingURL=nagrody-info.js.map

/***/ }),

/***/ 177:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MmBasketPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__mm__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MmBasketPage = (function () {
    function MmBasketPage(navCtrl, viewCtrl, navParams, events, toastController, alertController, authService) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.events = events;
        this.toastController = toastController;
        this.alertController = alertController;
        this.authService = authService;
        this.orders = [];
        this.ordersSum = 0;
        this.userData = { visitor_id: '', points: 0, visitor_number: '' };
        if (localStorage.getItem('basketData')) {
            var data = JSON.parse(localStorage.getItem('basketData'));
            if (data != null) {
                this.ordersSum = data.itemSum;
                this.orders = data.items;
            }
        }
        var data2 = JSON.parse(localStorage.getItem('userData'));
        this.userData.visitor_number = data2.visitor_number;
        this.userData.points = data2.visitor_points;
        this.userData.visitor_id = data2.visitor_id;
    }
    MmBasketPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MmPage');
    };
    MmBasketPage.prototype.removePosition = function (element) {
        var _this = this;
        var alert = this.alertController.create({
            title: 'Prośba o potwierdzenie',
            message: 'Czy na pewno chcesz usunąć pozycję ' + element.product.product_title + ' z koszyka?',
            buttons: [
                {
                    text: 'Anuluj',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Akceptuję',
                    handler: function () {
                        _this.orders = _this.orders.filter(function (item) { return item.id !== element.id; });
                        localStorage.setItem('basketData', JSON.stringify({
                            itemSum: _this.ordersSum - element.amount,
                            items: _this.orders
                        }));
                        var toast = _this.toastController.create({
                            message: 'Produkt został usunięy z koszyka',
                            duration: 2000
                        });
                        toast.present();
                    }
                }
            ]
        });
        alert.present();
    };
    MmBasketPage.prototype.makeOrder = function () {
        var _this = this;
        var alert = this.alertController.create({
            title: 'Prośba o potwierdzenie',
            message: 'Potwierdzam swoje zamówienie.',
            buttons: [
                {
                    text: 'Anuluj',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Potwierdzam',
                    handler: function () {
                        var order = {};
                        order.user_number = _this.userData.visitor_number;
                        order.orders = [];
                        for (var _i = 0, _a = _this.orders; _i < _a.length; _i++) {
                            var item = _a[_i];
                            order.orders.push({
                                product_id: item.product.mm_product_id,
                                amount: item.amount,
                                sum: item.product.product_points * item.amount
                            });
                        }
                        _this.authService.postData('orderSend', order, 'mm/new-order').then(function (result) {
                            _this.responseData = result;
                            if (_this.responseData.status == 'ok') {
                                var usedPoints = _this.responseData.used_points;
                                localStorage.setItem('basketData', null);
                                var data2 = JSON.parse(localStorage.getItem('userData'));
                                data2.visitor_points -= usedPoints;
                                localStorage.setItem('userData', JSON.stringify(data2));
                                _this.orders = [];
                                var toast = _this.toastController.create({
                                    message: 'Zamówienie zostało złożone. Wymienione punkty: ' + usedPoints + '. Dziękujemy.',
                                    duration: 2000
                                });
                                toast.present();
                            }
                        }, function (err) {
                        });
                        _this.backToMm();
                    }
                }
            ]
        });
        alert.present();
    };
    MmBasketPage.prototype.backToMm = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__mm__["a" /* MmPage */]);
        this.viewCtrl.dismiss();
    };
    MmBasketPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mm-basket',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\mm\tabs\basket.html"*/'<ion-content padding>\n\n  <div class="b-mainWrapper is-awards is-orders">\n\n    <div class="b-main">\n\n      <header class="b-header">\n\n        <a (click)="backToMm()" class="is-backBtn"><i class="icon-arrow02"></i> Miles & More</a>\n\n        <label for="menu"><i class="icon-menu01"></i></label>\n\n      </header>\n\n      <div class="b-awards">\n\n        <h1 class="b-headline">Moje zamówienie</h1>\n\n        <div class="b-orderList">\n\n          <div class="b-orderList_item" *ngFor="let item of orders">\n\n            <div class="is-name">\n\n              <a href="#">\n\n                {{ item.product.product_title }}\n\n              </a>\n\n            </div>\n\n            <div class="is-value">\n\n              {{ item.amount }} szt.\n\n            </div>\n\n            <div class="is-value">\n\n              {{ item.product.product_points*item.amount }} pkt.\n\n            </div>\n\n            <div class="is-remove">\n\n              <a (click)="removePosition(item)"><i class="icon-remove01"></i></a>\n\n            </div>\n\n          </div>\n\n        </div>\n\n        <div class="b-awards_footer">\n\n          <a (click)="makeOrder()" class="a-btn is-primary">\n\n            Zamów\n\n          </a>\n\n          <p>Do wykorzystania pozostało {{ userData.points }} pkt.</p>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\mm\tabs\basket.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */]])
    ], MmBasketPage);
    return MmBasketPage;
}());

//# sourceMappingURL=basket.js.map

/***/ }),

/***/ 178:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MmKryteriaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__mm__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MmKryteriaPage = (function () {
    function MmKryteriaPage(viewCtrl, navCtrl, authService, app, events) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.app = app;
        this.events = events;
        //m123451
        this.userPostData = { "visitor_id": "" };
        this.agenda = [];
    }
    MmKryteriaPage.prototype.backToMm = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__mm__["a" /* MmPage */]);
        this.viewCtrl.dismiss();
    };
    MmKryteriaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mm-kryteria',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\mm\tabs\kryteria.html"*/'<ion-content no-bounce>\n\n    <div class="b-mainWrapper is-rank">\n\n        <div class="b-main" style="margin-left: 0px; margin-right: 0px;">\n\n            <header class="b-header">\n\n                <a (click)="backToMm()" class="is-backBtn"><i class="icon-arrow02"></i> Miles & More</a>\n\n                <label for="menu"><i class="icon-menu01"></i></label>\n\n            </header>\n\n\n\n            <div class="b-content">\n\n                <div [innerHTML]="kryteria_desc | safeHtml">\n\n                    {{ kryteria_desc }}\n\n                </div>\n\n\n\n            </div>\n\n        </div>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\mm\tabs\kryteria.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], MmKryteriaPage);
    return MmKryteriaPage;
}());

//# sourceMappingURL=kryteria.js.map

/***/ }),

/***/ 225:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(245);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 245:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(294);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_welcome_welcome__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_agenda_agenda__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_ankieta_ankieta__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_kto_kto__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_noclegi_noclegi__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_score_score__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_test_test__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_news_news__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_tabs_tabs__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_test_page1__ = __webpack_require__(300);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_test_page2__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_test_page3__ = __webpack_require__(302);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_mm_mm__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_mm_tabs_kryteria__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_mm_tabs_nagrody__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_mm_tabs_nagrody_info__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_mm_tabs_ranking__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_mm_tabs_basket__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_status_bar__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_splash_screen__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pipes_safe_html_safe_html__ = __webpack_require__(303);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27_ionic_cache__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__angular_http__ = __webpack_require__(85);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






























//import { Network } from '@ionic-native/network';
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_agenda_agenda__["a" /* AgendaPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_ankieta_ankieta__["a" /* AnkietaPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_kto_kto__["a" /* KtoPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_welcome_welcome__["a" /* WelcomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_noclegi_noclegi__["a" /* NoclegiPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_news_news__["a" /* NewsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_test_test__["a" /* TestPage */], __WEBPACK_IMPORTED_MODULE_15__pages_test_page1__["a" /* Page1Page */], __WEBPACK_IMPORTED_MODULE_16__pages_test_page2__["a" /* Page2Page */], __WEBPACK_IMPORTED_MODULE_17__pages_test_page3__["a" /* Page3Page */],
                __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* Login */],
                __WEBPACK_IMPORTED_MODULE_14__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_mm_mm__["a" /* MmPage */], __WEBPACK_IMPORTED_MODULE_19__pages_mm_tabs_kryteria__["a" /* MmKryteriaPage */], __WEBPACK_IMPORTED_MODULE_20__pages_mm_tabs_nagrody__["a" /* MmNagrodyPage */], __WEBPACK_IMPORTED_MODULE_22__pages_mm_tabs_ranking__["b" /* MmRankingPage */], __WEBPACK_IMPORTED_MODULE_21__pages_mm_tabs_nagrody_info__["a" /* MmProductInfoPage */], __WEBPACK_IMPORTED_MODULE_23__pages_mm_tabs_basket__["a" /* MmBasketPage */], __WEBPACK_IMPORTED_MODULE_22__pages_mm_tabs_ranking__["a" /* MmRankingFiltryPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_score_score__["a" /* ScorePage */],
                __WEBPACK_IMPORTED_MODULE_26__pipes_safe_html_safe_html__["a" /* SafeHtmlPipe */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_28__angular_http__["HttpModule"],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/mm/mm.module#MmPageModule', name: 'MmPage', segment: 'mm', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_27_ionic_cache__["a" /* CacheModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_agenda_agenda__["a" /* AgendaPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_ankieta_ankieta__["a" /* AnkietaPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_kto_kto__["a" /* KtoPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_welcome_welcome__["a" /* WelcomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_noclegi_noclegi__["a" /* NoclegiPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_mm_mm__["a" /* MmPage */], __WEBPACK_IMPORTED_MODULE_19__pages_mm_tabs_kryteria__["a" /* MmKryteriaPage */], __WEBPACK_IMPORTED_MODULE_20__pages_mm_tabs_nagrody__["a" /* MmNagrodyPage */], __WEBPACK_IMPORTED_MODULE_22__pages_mm_tabs_ranking__["b" /* MmRankingPage */], __WEBPACK_IMPORTED_MODULE_21__pages_mm_tabs_nagrody_info__["a" /* MmProductInfoPage */], __WEBPACK_IMPORTED_MODULE_23__pages_mm_tabs_basket__["a" /* MmBasketPage */], __WEBPACK_IMPORTED_MODULE_22__pages_mm_tabs_ranking__["a" /* MmRankingFiltryPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_news_news__["a" /* NewsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_test_test__["a" /* TestPage */], __WEBPACK_IMPORTED_MODULE_15__pages_test_page1__["a" /* Page1Page */], __WEBPACK_IMPORTED_MODULE_16__pages_test_page2__["a" /* Page2Page */], __WEBPACK_IMPORTED_MODULE_17__pages_test_page3__["a" /* Page3Page */],
                __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* Login */],
                __WEBPACK_IMPORTED_MODULE_14__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_score_score__["a" /* ScorePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__["a" /* AuthService */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
                __WEBPACK_IMPORTED_MODULE_22__pages_mm_tabs_ranking__["b" /* MmRankingPage */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 294:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_agenda_agenda__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_ankieta_ankieta__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_kto_kto__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_welcome_welcome__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_auth_service_auth_service__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_noclegi_noclegi__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_score_score__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_test_test__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_news_news__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_ionic_cache__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_mm_mm__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















var MyApp = (function () {
    function MyApp(platform, statusBar, cache, splashScreen, app, authService, menuCtrl, loadingCtrl, events) {
        var _this = this;
        this.app = app;
        this.authService = authService;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.events = events;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* Login */];
        this.footerIsHidden = false;
        this.siteTitle = 'NSM 2018';
        this.siteData = { ankietyName: "", ankietyActive: false };
        platform.ready().then(function () {
            cache.setDefaultTTL(60 * 60 * 12); //12h cache
            cache.setOfflineInvalidate(false);
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            statusBar.overlaysWebView(false);
            splashScreen.hide();
            _this.notificationOpenedCallback = function (jsonData) {
                //var notificationOpenedCallback = function(jsonData) {
                console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
                if (jsonData.notification.payload.additionalData != null) {
                    console.log("Here we access addtional data");
                    if (jsonData.notification.payload.additionalData.news_id != null) {
                        //this.goToNav('tabWelcome');
                        console.log('Here we access the news_id sent in the notification data');
                        //events.subscribe('root:created', (data) => {
                        _this.goToNav('tabNews', jsonData.notification.payload.additionalData.news_id);
                        //events.unsubscribe('root:created');
                        //});
                    }
                }
            };
            /*  window["plugins"].OneSignal
                  .startInit("73b5d8a1-0f4f-481a-8d2b-cdb8956f9569")
                  .handleNotificationOpened(this.notificationOpenedCallback)
                  .endInit();*/
        });
        this.authService.postData('tabTextSite', {}, 'visitor/check2').then(function (result) {
            _this.responseData = result;
            localStorage.setItem('siteData', JSON.stringify(_this.responseData));
            _this.siteData = _this.responseData;
        }, function (err) {
            console.log('Error' + err);
        });
        events.subscribe('hideHeader', function (data) {
            _this.footerIsHidden = data.isHidden;
        });
        events.subscribe('setSiteTitle', function (data) {
            _this.siteTitle = data.title;
        });
    }
    MyApp.prototype.presentLoadingDefault = function () {
        var loading = this.loadingCtrl.create({
            content: 'Proszę czekać...'
        });
        loading.present();
        setTimeout(function () {
            loading.dismiss();
        }, 1000);
    };
    MyApp.prototype.goToNav = function (action, news_id) {
        var _this = this;
        if (news_id === void 0) { news_id = null; }
        if (action == 'tabAgenda') {
            this.presentLoadingDefault();
            var startIndex_1 = this.nav.getActive().index;
            this.menuCtrl.close();
            this.nav.push(__WEBPACK_IMPORTED_MODULE_4__pages_agenda_agenda__["a" /* AgendaPage */]).then(function () {
                _this.nav.remove(startIndex_1);
            });
            this.events.publish('setSiteTitle', { title: 'Agenda' });
        }
        if (action == 'tabAnkieta') {
            this.presentLoadingDefault();
            var startIndex_2 = this.nav.getActive().index;
            this.menuCtrl.close();
            this.nav.push(__WEBPACK_IMPORTED_MODULE_5__pages_ankieta_ankieta__["a" /* AnkietaPage */]).then(function () {
                _this.nav.remove(startIndex_2);
            });
            this.events.publish('setSiteTitle', { title: 'Ankiety' });
        }
        if (action == 'tabKto') {
            this.presentLoadingDefault();
            var startIndex_3 = this.nav.getActive().index;
            this.menuCtrl.close();
            this.nav.push(__WEBPACK_IMPORTED_MODULE_7__pages_kto_kto__["a" /* KtoPage */]).then(function () {
                _this.nav.remove(startIndex_3);
            });
            this.events.publish('setSiteTitle', { title: 'Kto jest kto' });
        }
        if (action == 'tabWelcome') {
            this.presentLoadingDefault();
            var startIndex_4 = this.nav.getActive().index;
            this.menuCtrl.close();
            this.nav.push(__WEBPACK_IMPORTED_MODULE_8__pages_welcome_welcome__["a" /* WelcomePage */]).then(function () {
                _this.nav.remove(startIndex_4);
            });
            this.events.publish('setSiteTitle', { title: 'NSM 2018' });
        }
        if (action == 'tabNoclegi') {
            this.presentLoadingDefault();
            var startIndex_5 = this.nav.getActive().index;
            this.menuCtrl.close();
            this.nav.push(__WEBPACK_IMPORTED_MODULE_10__pages_noclegi_noclegi__["a" /* NoclegiPage */]).then(function () {
                _this.nav.remove(startIndex_5);
            });
            this.events.publish('setSiteTitle', { title: 'Noclegi' });
        }
        if (action == 'tabScore') {
            this.presentLoadingDefault();
            var startIndex_6 = this.nav.getActive().index;
            this.menuCtrl.close();
            this.nav.push(__WEBPACK_IMPORTED_MODULE_11__pages_score_score__["a" /* ScorePage */]).then(function () {
                _this.nav.remove(startIndex_6);
            });
            this.events.publish('setSiteTitle', { title: 'Cele' });
        }
        if (action == 'tabTest') {
            this.presentLoadingDefault();
            var startIndex_7 = this.nav.getActive().index;
            this.menuCtrl.close();
            this.nav.push(__WEBPACK_IMPORTED_MODULE_12__pages_test_test__["a" /* TestPage */]).then(function () {
                _this.nav.remove(startIndex_7);
            });
            this.events.publish('setSiteTitle', { title: this.siteData.ankietyName });
        }
        if (action == 'tabMm') {
            this.presentLoadingDefault();
            var startIndex_8 = this.nav.getActive().index;
            this.menuCtrl.close();
            this.nav.push(__WEBPACK_IMPORTED_MODULE_15__pages_mm_mm__["a" /* MmPage */]).then(function () {
                _this.nav.remove(startIndex_8);
            });
            this.events.publish('setSiteTitle', { title: 'M&M' });
        }
        if (action == 'tabNews') {
            this.presentLoadingDefault();
            var startIndex_9 = this.nav.getActive().index;
            this.menuCtrl.close();
            // news_id = 6;
            if (news_id != null) {
                this.nav.push(__WEBPACK_IMPORTED_MODULE_13__pages_news_news__["a" /* NewsPage */], {
                    param_news_id: news_id
                }).then(function () {
                    _this.nav.remove(startIndex_9);
                });
            }
            else {
                this.nav.push(__WEBPACK_IMPORTED_MODULE_13__pages_news_news__["a" /* NewsPage */]).then(function () {
                    _this.nav.remove(startIndex_9);
                });
            }
            this.events.publish('setSiteTitle', { title: 'Aktualności' });
        }
    };
    MyApp.prototype.logout = function () {
        localStorage.setItem('userData', null);
        this.presentLoadingDefault();
        this.menuCtrl.close();
        this.app.getActiveNav().push(__WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* Login */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('mycontent'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-homeapp',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\app\app.html"*/'<ion-menu side="right" [content]="mycontent">\n\n    <ion-content>\n\n        <ion-list>\n\n\n\n<ion-list-header style="margin-top: 20px; border-top: none">\n\n                Menu\n\n            </ion-list-header>\n\n\n\n            <ion-item style="font-size: 1.1rem" (click)="goToNav(\'tabWelcome\')">\n\n                <ion-icon name=\'home\' item-start></ion-icon>\n\n                Home\n\n            </ion-item>\n\n\n\n            <ion-item style="font-size: 1.1rem" (click)="goToNav(\'tabAgenda\')">\n\n                <ion-icon name=\'list-box\' item-start></ion-icon>\n\n                Agenda\n\n            </ion-item>\n\n\n\n            <ion-item style="font-size: 1.1rem" (click)="goToNav(\'tabTest\')" *ngIf="!siteData.ankietyActive">\n\n                <ion-icon name=\'stats\' item-start></ion-icon>\n\n                {{siteData.ankietyName}}\n\n            </ion-item>\n\n\n\n            <ion-item style="font-size: 1.1rem" (click)="goToNav(\'tabAnkieta\')" *ngIf="siteData.ankietyActive">\n\n                <ion-icon name=\'stats\' item-start></ion-icon>\n\n                Ankiety\n\n            </ion-item>\n\n\n\n            <ion-item style="font-size: 1.1rem" (click)="goToNav(\'tabKto\')">\n\n                <ion-icon name=\'contact\' item-start></ion-icon>\n\n                Kto jest kto\n\n            </ion-item>\n\n\n\n            <!--<ion-item style="font-size: 1.1rem" (click)="goToNav(\'tabNoclegi\')">\n\n                <ion-icon name=\'moon\' item-start></ion-icon>\n\n                Noclegi\n\n            </ion-item>-->\n\n\n\n            <ion-item style="font-size: 1.1rem" (click)="goToNav(\'tabNews\')">\n\n                <ion-icon name=\'bookmarks\' item-start></ion-icon>\n\n                Aktualności\n\n            </ion-item>\n\n\n\n            <ion-item style="font-size: 1.1rem" (click)="goToNav(\'tabScore\')">\n\n                <ion-icon name=\'checkbox\' item-start></ion-icon>\n\n                Cele\n\n            </ion-item>\n\n\n\n            <ion-item style="font-size: 1.1rem" (click)="goToNav(\'tabMm\')">\n\n                <ion-icon name=\'checkbox\' item-start></ion-icon>\n\n                M&M\n\n            </ion-item>\n\n\n\n            <ion-item style="font-size: 1.1rem" (click)="logout()">\n\n                <ion-icon name=\'exit\' item-start></ion-icon>\n\n                Wyloguj\n\n            </ion-item>\n\n\n\n        </ion-list>\n\n    </ion-content>\n\n</ion-menu>\n\n\n\n<ion-header *ngIf="!footerIsHidden" no-shadow no-border>\n\n\n\n    <ion-navbar class="c-form a-typo is-primary" style="font-size: 1.2rem; margin-top: 13px;" no-shadow no-border>\n\n        <span class="titleMenu" style="margin-left: 10px;  color: #333; font-size: 1.8rem; line-height: 40px;">{{ siteTitle }}</span>\n\n        <ion-buttons end>\n\n        <button ion-button icon-only menuToggle style="color: #00BBE6;">\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        </ion-buttons>\n\n\n\n        <!--<button class="c-form_btn" (tap)="logout()" tappable="" style="float: right; font-size: 14px; margin-right: 10px; margin-top: 2px; height: 30px; line-height: 30px; min-width: 90px; border-radius: 16px;">Wyloguj</button>-->\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-nav #mycontent [root]="rootPage" no-bounce></ion-nav>\n\n\n\n'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_14_ionic_cache__["b" /* CacheService */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_9__providers_auth_service_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 299:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TabsPage = (function () {
    function TabsPage(authService, viewCtr, navCtr, app) {
        this.authService = authService;
        this.viewCtr = viewCtr;
        this.navCtr = navCtr;
        this.app = app;
    }
    TabsPage.prototype.goToNav = function (action) {
        if (action == 'tabAgenda') {
            //var model = new AgendaPage(this.viewCtr, this.navCtr, this.authService, this.app);
            //model.goToNav(action);
        }
    };
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\tabs\tabs.html"*/'<ion-tabs>\n\n  <ion-tab [root]="tab1Root" tabTitle="PureScanner" tabIcon="home"></ion-tab>\n\n  <ion-tab [root]="tab2Root" tabTitle="Wystawcy" tabIcon="information-circle"></ion-tab>\n\n  <ion-tab [root]="tab3Root" tabTitle="Kontakt" tabIcon="contacts"></ion-tab>\n\n</ion-tabs>\n\n'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\tabs\tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 300:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Page1Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Page1Page = (function () {
    function Page1Page(navCtrl, viewCtrl, authService, app) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.authService = authService;
        this.app = app;
        this.userPostData = { "visitor_id": "", "answer_id": [] };
        this.ankiety = [];
    }
    Page1Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-test2',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\test\page1.html"*/'<div class="b-main" style="font-size: 16px; padding-top: 50px;">\n\n    <div class="c-form_row">\n\n        <p class="c-form_headline" style="margin-top: 20px;">Możesz rozpocząć test wiedzy z zakresu naszego CSR’u. Możesz go\n\n            rozwiązać TYLKO JEDEN RAZ. Test składa się z x pytań. Od momentu kliknięcia przycisku\n\n            „start” mierzony jest sumaryczny czas rozwiązania testu – w sytuacji gdy więcej niż jedna\n\n            osoba będzie miała taką samą ilość poprawnych odpowiedzi jako kolejne kryterium brany\n\n            będzie pod uwagę czas.<br />\n\n            Niektóre pytania zawierają treść video – aby ją obejrzeć należy kliknąć przycisk "play".\n\n            W każdym pytaniu tylko jedna odpowiedź jest poprawna.</p>\n\n    </div>\n\n\n\n</div>\n\n'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\test\page1.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */]])
    ], Page1Page);
    return Page1Page;
}());

//# sourceMappingURL=page1.js.map

/***/ }),

/***/ 301:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Page2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Page2Page = (function () {
    function Page2Page(navCtrl, viewCtrl, authService, app, alertCtrl, menu) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.authService = authService;
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.userPostData = { "visitor_id": "", "answer_id": [] };
        this.start = false;
        this.startTime = 0;
        this.votes = [];
        menu.enable(true);
        var data = JSON.parse(localStorage.getItem('userData'));
        this.userPostData.visitor_id = data.visitor_id;
        this.authService.postData('ankietaList', this.userPostData, 'polltest/get').then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                _this.ankiety = JSON.parse(_this.responseData.polls);
                _this.txt = JSON.parse(_this.responseData.txt);
                var ii = 0;
                for (var poll in _this.ankiety) {
                    _this.votes.push(0);
                }
                console.log(_this.ankiety);
                console.log(_this.votes);
            }
        }, function (err) {
            console.log('Error' + err);
        });
    }
    Page2Page.prototype.startVote = function () {
        this.start = true;
        this.startTime = Date.now();
        console.log(this.startTime);
    };
    Page2Page.prototype.vote = function () {
        var _this = this;
        console.log(Date.now());
        var time = Date.now() - this.startTime;
        this.authService.postData('ankietaVote', this.votes, 'polltest/vote/' + this.userPostData.visitor_id + '/' + time).then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                _this.presentAlert();
                _this.navCtrl.parent.select(2);
                _this.ankiety = false;
            }
            else {
                alert('Wystąpił błąd podczas oddawania głosu.');
            }
        }, function (err) {
            console.log('Error' + err);
        });
    };
    Page2Page.prototype.presentAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Dziękujemy za oddanie głosu',
            subTitle: '',
            buttons: ['Zamknij']
        });
        alert.present();
    };
    Page2Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-test3',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\test\page2.html"*/'<div class="b-mainWrapper is-category">\n\n\n\n    <div class="b-main" style="font-size: 16px; padding-top: 50px;">\n\n        <div>\n\n            <div class="c-form">\n\n                <div class="c-form_row" *ngIf="!ankiety">\n\n                    <p class="c-form_headline" style="margin-top: 20px;">Etap został zakończony. Sprawdź swój ranking w zakładce Ranking.</p>\n\n                </div>\n\n            </div>\n\n            <div class="c-form" *ngIf="ankiety && !start" style="text-align: center">\n\n                <div [innerHTML]="txt | safeHtml">\n\n                    {{ txt }}\n\n                </div>\n\n                <div [innerHTML]="txt3 | safeHtml">\n\n                    {{ txt2 }}\n\n                </div>\n\n                <button (click)="startVote()" class="c-form_btn" style="margin-top: 50px;">PLAY</button>\n\n            </div>\n\n            <div *ngIf="ankiety && start">\n\n                <div class="c-form" *ngFor="let item of ankiety; let i = index">\n\n                    <div class="c-form_row">\n\n                        <p class="c-form_headline" style="margin-top: 20px; font-weight: bold">{{ item.question[0].question_title }}</p>\n\n                        <img *ngIf="item.question[0].image" src="http://api-ios.test001.pl/data/ankieta/{{ item.polling_id }}.jpg" class="" />\n\n                        <video *ngIf="item.question[0].video" style="width: 100%; height: 100%;" class="video-player" controls>\n\n                            <source src="http://api-ios.test001.pl/data/video/{{ item.polling_id }}.mp4" type="video/mp4" />\n\n                        </video>\n\n                    </div>\n\n                    <div class="c-form_row" *ngFor="let item_answer of item.answer">\n\n                        <label class="c-form_radio">\n\n                            <input type="radio" name="radio1" [(ngModel)]="votes[i]"\n\n                                   [ngModelOptions]="{standalone: true}" [value]="item_answer.polling_answer_id">\n\n                            <span class="{{ item_answer.answer_letter }}"><i>{{ item_answer.answer_letter }}</i>{{ item_answer.answer_title }}</span>\n\n                        </label>\n\n                    </div>\n\n                </div>\n\n                <div class="c-form c-form_row is-submit" style="text-align: center;">\n\n                    <button (click)="vote()" class="c-form_btn" style="margin-top: 10px; margin-bottom: 100px;">Wyślij</button>\n\n                </div>\n\n            </div>\n\n        </div>\n\n    </div>\n\n</div>'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\test\page2.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */]])
    ], Page2Page);
    return Page2Page;
}());

//# sourceMappingURL=page2.js.map

/***/ }),

/***/ 302:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Page3Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Page3Page = (function () {
    function Page3Page(navCtrl, viewCtrl, authService, app, menu) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.authService = authService;
        this.app = app;
        this.userPostData = { "visitor_id": "" };
        this.ranking = [];
        menu.enable(true);
        var data = JSON.parse(localStorage.getItem('userData'));
        this.userPostData.visitor_id = data.visitor_id;
        /*this.authService.postData('rankingList', this.userPostData,'polltest/rank').then((result) => {
            this.responseData = result;

            if(this.responseData.status == 'ok') {
                this.ranking = JSON.parse(this.responseData);

                // for(var key in json)
                //   if(json.hasOwnProperty(key))
                //     this.ankiety.push([key, json[key]]); // each item is an array in format [key, value]

                console.log(this.ranking);

            }
        }, (err) => {
            console.log('Error' + err);
        });*/
        this.authService.postData('rankingListUser', this.userPostData, 'polltest/rank/' + this.userPostData.visitor_id).then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                _this.rankingUser = JSON.parse(_this.responseData.aswer);
                // for(var key in json)
                //   if(json.hasOwnProperty(key))
                //     this.ankiety.push([key, json[key]]); // each item is an array in format [key, value]
                console.log(_this.rankingUser);
            }
        }, function (err) {
            console.log('Error' + err);
        });
    }
    Page3Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-test4',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\test\page3.html"*/'<div class="b-main" style="font-size: 16px; padding-top: 50px;">\n\n    <div *ngIf="rankingUser">\n\n        <p>Dziękujemy za wypełnienie testu</p>\n\n        Liczba Twoich poprawnych odpowiedzi: {{rankingUser.odpowiedzi}}<br />\n\n        Twój czas: {{rankingUser.czas}}<br />\n\n        Aktualnie zajmujesz {{rankingUser.miejsce}} miejsce\n\n    </div>\n\n    <div *ngIf="!rankingUser">\n\n        Prosimy o wypełnienie quizu\n\n    </div>\n\n</div>\n\n'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\test\page3.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */]])
    ], Page3Page);
    return Page3Page;
}());

//# sourceMappingURL=page3.js.map

/***/ }),

/***/ 303:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SafeHtmlPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SafeHtmlPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var SafeHtmlPipe = (function () {
    function SafeHtmlPipe(sanitizer) {
        this.sanitizer = sanitizer;
    }
    SafeHtmlPipe.prototype.transform = function (html) {
        return this.sanitizer.bypassSecurityTrustHtml(html);
    };
    SafeHtmlPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'safeHtml',
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */]])
    ], SafeHtmlPipe);
    return SafeHtmlPipe;
}());

//# sourceMappingURL=safe-html.js.map

/***/ }),

/***/ 31:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MmPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_ranking__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__tabs_kryteria__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabs_nagrody__ = __webpack_require__(87);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the MmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MmPage = (function () {
    function MmPage(navCtrl, viewCtrl, navParams, events) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.events = events;
        this.events.publish('hideHeader', { isHidden: true });
    }
    MmPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MmPage');
    };
    MmPage.prototype.goToNav = function (action) {
        if (action == 'ranking') {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__tabs_ranking__["b" /* MmRankingPage */]);
        }
        if (action == 'kryteria') {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__tabs_kryteria__["a" /* MmKryteriaPage */]);
        }
        if (action == 'nagrody') {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__tabs_nagrody__["a" /* MmNagrodyPage */]);
        }
        this.viewCtrl.dismiss();
    };
    MmPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mm',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\mm\mm.html"*/'<ion-content padding>\n\n  <div class="b-mainWrapper is-mm">\n\n    <div class="b-main">\n\n      <header class="b-header">\n\n        <button class="is-backBtn"><i class="icon-arrow02"></i></button>\n\n        <label for="menu"><i class="icon-menu01"></i></label>\n\n      </header>\n\n      <nav class="b-mmNav">\n\n        <ul>\n\n          <li>\n\n            <a (click)="goToNav(\'ranking\')">Ranking</a>\n\n          </li>\n\n          <li>\n\n            <a (click)="goToNav(\'kryteria\')">Kryteria</a>\n\n          </li>\n\n          <li>\n\n            <a (click)="goToNav(\'nagrody\')">Nagrody</a>\n\n          </li>\n\n        </ul>\n\n      </nav>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\mm\mm.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], MmPage);
    return MmPage;
}());

//# sourceMappingURL=mm.js.map

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AgendaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ankieta_ankieta__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__kto_kto__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_jquery__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_jquery__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AgendaPage = (function () {
    function AgendaPage(viewCtrl, navCtrl, authService, app, events) {
        var _this = this;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.app = app;
        this.events = events;
        //m123451
        this.userPostData = { "visitor_id": "" };
        this.agenda = [];
        this.events.publish('hideHeader', { isHidden: false });
        var data = JSON.parse(localStorage.getItem('userData'));
        this.userPostData.visitor_id = data.visitor_id;
        this.authService.postData('agendaList', this.userPostData, 'agenda/get-list/').then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                var json = JSON.parse(_this.responseData.agenda);
                for (var key in json)
                    if (json.hasOwnProperty(key))
                        _this.agenda.push([key, json[key]]); // each item is an array in format [key, value]
            }
        }, function (err) {
        });
    }
    AgendaPage_1 = AgendaPage;
    AgendaPage.prototype.toggleBox = function (id) {
        __WEBPACK_IMPORTED_MODULE_6_jquery__('#box_full_' + id).toggle();
        __WEBPACK_IMPORTED_MODULE_6_jquery__('#box_short_' + id).toggle();
        __WEBPACK_IMPORTED_MODULE_6_jquery__('#click_full_' + id).parent().parent().parent().toggleClass('is-active');
        __WEBPACK_IMPORTED_MODULE_6_jquery__('#click_full_' + id).toggleClass('icon-more01');
    };
    AgendaPage.prototype.toggleSection = function (i) {
        if (!this.agenda[i][1]['active']) {
            for (var _i = 0, _a = this.agenda; _i < _a.length; _i++) {
                var a = _a[_i];
                a[1]['active'] = false;
            }
            ;
        }
        this.agenda[i][1]['active'] = !this.agenda[i][1]['active'];
    };
    AgendaPage.prototype.goToNav = function (action) {
        if (action == 'tabAgenda') {
            this.navCtrl.push(AgendaPage_1);
        }
        if (action == 'tabAnkieta') {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__ankieta_ankieta__["a" /* AnkietaPage */]);
        }
        if (action == 'tabKto') {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__kto_kto__["a" /* KtoPage */]);
        }
        this.viewCtrl.dismiss();
    };
    AgendaPage.prototype.logout = function () {
        localStorage.setItem('userData', null);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* Login */]);
        this.viewCtrl.dismiss();
    };
    AgendaPage = AgendaPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-agenda',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\agenda\agenda.html"*/'<ion-content no-bounce scrollbar-y="false">\n\n<div class="b-mainWrapper is-category">\n\n    <div class="b-main">\n\n        <ion-list class="accordion-list c-accordion">\n\n            <!-- First Level -->\n\n            <ion-list-header *ngFor="let item of agenda; let i = index" no-lines no-padding [ngClass]="{\'is-active\': item[1][\'active\'], \'\': !item[1][\'active\']}">\n\n                <h2 class="c-accordion_title js-accordion_title" id="js-accordion_title_{{ item[0] }}" (click)="toggleSection(i)" detail-none style="letter-spacing: 0px; text-transform: none; font-size: 16px; font-family: Arial">\n\n                    {{ item[1].dzien }} - {{ item[0] }}\n\n                    <i class="icon-arrow02"></i>\n\n                </h2>\n\n\n\n                <ion-list *ngIf="item[1].agenda && item[1][\'active\']" no-lines>\n\n                    <ion-list-header *ngFor="let row of item[1].agenda; let j = index" no-padding class="c-accordion_content" style="border-top: 0px;">\n\n                        <article>\n\n                            <header class="c-accordion_header">\n\n                                <span class="c-accordion_data" style="white-space: normal; letter-spacing: 0px; text-transform: none; font-size: 0.9rem; font-weight: 400; font-family: Arial">\n\n                                    <i class="icon-clock01"></i>\n\n                                    {{ row.agenda_hour_start }} - {{ row.agenda_hour_stop }}\n\n                                </span>\n\n                                        <span *ngIf="row.agenda_place != \'\'" class="c-accordion_place" style="white-space: normal; letter-spacing: 0px; text-transform: none; font-size: 0.9rem; font-family: Arial">\n\n                                    Lokalizacja: <span style="white-space: normal; letter-spacing: 0px; text-transform: none; font-size: 0.9rem; font-weight: 400; font-family: Arial">{{ row.agenda_place }}</span>\n\n                                </span>\n\n                            </header>\n\n                            <article class="c-accordion_text">\n\n                                <h3 style="white-space: normal; letter-spacing: 0px; text-transform: none; font-size: 1rem; font-weight: 700; font-family: Arial">{{ row.agenda_title }}</h3>\n\n                                <p *ngIf="row.agenda_desc != \'\'" id="box_short_{{ row.agenda_id }}" style="white-space: normal; letter-spacing: 0px; font-size: 0.9rem; text-transform: none; font-family: Arial">{{ row.agenda_desc\n\n                                    }}</p>\n\n                                <p *ngIf="row.agenda_desc_full != \'\'" style="white-space: normal; letter-spacing: 0px; font-size: 0.9rem; display: none; text-transform: none; letter-spacing: 0px; font-family: Arial"\n\n                                   id="box_full_{{ row.agenda_id }}">{{ row.agenda_desc_full }}</p>\n\n                            </article>\n\n                            <footer class="c-accordion_footer" *ngIf="row.members.length > 0">\n\n                                <p class="c-accordion_lead" style="white-space: normal; line-height: 1.2rem; letter-spacing: 0px; text-transform: none; font-size: 0.9rem; font-family: Arial">Prowadzący:</p>\n\n                                <p  style="white-space: normal; line-height: 1.2rem;"><span *ngFor="let member of row.members;  let last = last"><span\n\n                                        *ngIf="userPostData.visitor_id == member.visitor_id"\n\n                                        style="white-space: normal; letter-spacing: 0px; color: red; font-weight: bold; text-transform: none; font-size: 0.9rem; font-family: Arial">{{ member.visitor_firstname }} {{member.visitor_lastname }}</span><span\n\n                                        *ngIf="userPostData.visitor_id != member.visitor_id" style="white-space: normal; letter-spacing: 0px; text-transform: none; font-weight: 400; font-size: 0.9rem; font-family: Arial">{{ member.visitor_firstname }} {{member.visitor_lastname }}</span><span\n\n                                        *ngIf="!last" style="white-space: normal; letter-spacing: 0px; text-transform: none; font-weight: 400; font-size: 0.9rem; font-family: Arial">,</span> </span></p>\n\n                            </footer>\n\n                            <span *ngIf="row.agenda_desc_full != \'\'" class="c-accordion_add" style="cursor: pointer;"><i\n\n                                    (click)="toggleBox(row.agenda_id)" class="icon-more01 icon-close01"\n\n                                    id="click_full_{{ row.agenda_id }}"></i></span>\n\n                        </article>\n\n                    </ion-list-header>\n\n                </ion-list>\n\n            </ion-list-header>\n\n        </ion-list>\n\n    </div>\n\n</div>\n\n</ion-content>'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\agenda\agenda.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], AgendaPage);
    return AgendaPage;
    var AgendaPage_1;
}());

//# sourceMappingURL=agenda.js.map

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AnkietaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__agenda_agenda__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__kto_kto__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_jquery__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_jquery__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AnkietaPage = (function () {
    function AnkietaPage(navCtrl, viewCtrl, authService, app, alertCtrl, menu, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.authService = authService;
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.userPostData = { "visitor_id": "", "answer_id": [] };
        this.ankiety = [];
        this.events.publish('hideHeader', { isHidden: false });
        menu.enable(true);
        var data = JSON.parse(localStorage.getItem('userData'));
        this.userPostData.visitor_id = data.visitor_id;
        this.authService.postData('ankietaList', this.userPostData, 'poll/get').then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                _this.ankiety = JSON.parse(_this.responseData.polls);
                // for(var key in json)
                //   if(json.hasOwnProperty(key))
                //     this.ankiety.push([key, json[key]]); // each item is an array in format [key, value]
                console.log(_this.ankiety);
            }
        }, function (err) {
            console.log('Error' + err);
        });
    }
    AnkietaPage_1 = AnkietaPage;
    AnkietaPage.prototype.start = function (poll_id) {
        var _this = this;
        this.authService.postData('ankietaStart', this.userPostData, 'poll/activate/' + poll_id).then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                _this.navCtrl.push(AnkietaPage_1);
                _this.viewCtrl.dismiss();
            }
        }, function (err) {
            console.log('Error' + err);
        });
    };
    AnkietaPage.prototype.stop = function (poll_id) {
        var _this = this;
        this.authService.postData('ankietaStop', this.userPostData, 'poll/deactivate/' + poll_id).then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                _this.navCtrl.push(AnkietaPage_1);
                _this.viewCtrl.dismiss();
            }
        }, function (err) {
            console.log('Error' + err);
        });
    };
    AnkietaPage.prototype.results = function (poll_id) {
        __WEBPACK_IMPORTED_MODULE_5_jquery__('#results_' + poll_id).slideToggle();
    };
    AnkietaPage.prototype.vote = function (poll_id) {
        var _this = this;
        this.authService.postData('ankietaVote', this.userPostData, 'poll/vote/' + poll_id).then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                _this.presentAlert();
                _this.navCtrl.push(AnkietaPage_1);
                _this.viewCtrl.dismiss();
            }
            else {
                alert('Wystąpił błąd podczas oddawania głosu.');
            }
        }, function (err) {
            console.log('Error' + err);
        });
    };
    AnkietaPage.prototype.goToNav = function (action) {
        if (action == 'tabAgenda') {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__agenda_agenda__["a" /* AgendaPage */]);
        }
        if (action == 'tabAnkieta') {
            this.navCtrl.push(AnkietaPage_1);
        }
        if (action == 'tabKto') {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__kto_kto__["a" /* KtoPage */]);
        }
        this.viewCtrl.dismiss();
    };
    AnkietaPage.prototype.presentAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Dziękujemy za oddanie głosu',
            subTitle: '',
            buttons: ['Zamknij']
        });
        alert.present();
    };
    AnkietaPage = AnkietaPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-ankieta',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\ankieta\ankieta.html"*/'<ion-content no-bounce scrollbar-y="false">\n\n<div class="b-mainWrapper is-category">\n\n    <div class="b-main" style="font-size: 16px;">\n\n        <div class="c-form c-accordion" *ngIf="ankiety.owner_active == 1">\n\n            <div *ngFor="let item of ankiety.owner;" class="c-accordion_item is-active">\n\n                <div class="c-form_row c-accordion_content">\n\n                    <p class="c-form_headline" style="margin-top: 20px; color: #797979">{{ item.agenda_title }} - {{ item.agenda_day }} ({{ item.agenda_hour_start }}-{{ item.agenda_hour_stop }})</p>\n\n                    <span>{{ item.question[0].question_title }}</span>\n\n                    <div *ngIf="item.polling_active == 0">\n\n                        <button (tap)="start(item.polling_id)" class="c-form_btn"\n\n                                style="font-size: 13px; margin-top: 5px; height: 30px; line-height: 30px; min-width: 90px; border-radius: 16px;">\n\n                            Uruchom\n\n                        </button>\n\n                    </div>\n\n                    <div *ngIf="item.polling_active == 1">\n\n                        <button (tap)="stop(item.polling_id)" class="c-form_btn"\n\n                                style="font-size: 13px; margin-top: 5px; height: 30px; line-height: 30px; min-width: 90px; border-radius: 16px; background-color: red;">\n\n                            Zakończ\n\n                        </button>\n\n                    </div>\n\n                    <div *ngIf="item.polling_active == 2">\n\n                        <button (tap)="results(item.polling_id)" class="c-form_btn"\n\n                                style="font-size: 13px; margin-top: 5px; height: 30px; line-height: 30px; min-width: 90px; border-radius: 16px; background-color: blue;">\n\n                            Wyniki\n\n                        </button>\n\n                    </div>\n\n                </div>\n\n                <div class="c-form_row polling_result c-accordion_content" style="display: none;"\n\n                     id="results_{{item.polling_id}}" *ngIf="item.polling_active == 2">\n\n                    <p>Pytanie: <strong>{{ item.question[0].question_title }} (głosów {{ item.glosow }})</strong></p>\n\n                    <div *ngFor="let item_answer of item.answer;">\n\n                        <p>{{ item_answer.answer_title }} - {{ item_answer.glosow }}% ({{ item_answer.glosow_int }})</p>\n\n                    </div>\n\n                </div>\n\n            </div>\n\n        </div>\n\n        <div *ngIf="ankiety.owner_active == 0">\n\n            <div class="c-form">\n\n                <div class="c-form_row" *ngIf="!ankiety.active">\n\n                    <p class="c-form_headline" style="margin-top: 20px;">W tej chwili nie masz żadnego pytania</p>\n\n                </div>\n\n            </div>\n\n            <div class="c-form" *ngFor="let item of ankiety.active;">\n\n                <div class="c-form_row">\n\n                    <p class="c-form_headline" style="margin-top: 20px; font-weight: bold">{{ item.question[0].question_title }}</p>\n\n                </div>\n\n                <div class="c-form_row" *ngFor="let item_answer of item.answer; let i = index">\n\n                    <label class="c-form_radio" *ngIf="item.question[0].multi_answer == 0">\n\n                        <input type="radio" name="radio1" [(ngModel)]="userPostData.answer_id"\n\n                               [ngModelOptions]="{standalone: true}" value="{{item_answer.polling_answer_id}}">\n\n                        <span>{{ item_answer.answer_title }}</span>\n\n                    </label>\n\n\n\n                    <label class="c-form_radio" *ngIf="item.question[0].multi_answer == 1">\n\n                        <input type="checkbox" [(ngModel)]="userPostData.answer_id[item_answer.polling_answer_id]"\n\n                               value="{{item_answer.polling_answer_id}}">\n\n                        <span>{{ item_answer.answer_title }}</span>\n\n                    </label>\n\n                </div>\n\n                <div class="c-form_row is-submit">\n\n                    <button (click)="vote(item.polling_id)" class="c-form_btn">Wyślij</button>\n\n                </div>\n\n            </div>\n\n        </div>\n\n    </div>\n\n</div>\n\n</ion-content>'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\ankieta\ankieta.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], AnkietaPage);
    return AnkietaPage;
    var AnkietaPage_1;
}());

//# sourceMappingURL=ankieta.js.map

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KtoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__agenda_agenda__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ankieta_ankieta__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var KtoPage = (function () {
    function KtoPage(navCtrl, viewCtrl, authService, app, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.authService = authService;
        this.app = app;
        this.events = events;
        this.whoiswho = [];
        this.userPostData = { "comapny_id": "", "comapny_account": "", "company_name": "", "place": "", "status": "" };
        this.events.publish('hideHeader', { isHidden: false });
        this.authService.postData('ktoList', this.userPostData, 'visitor/whoiswho').then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                var json = JSON.parse(_this.responseData.userData);
                for (var key in json)
                    if (json.hasOwnProperty(key))
                        _this.whoiswho.push([key, json[key]]); // each item is an array in format [key, value]
            }
        }, function (err) {
        });
    }
    KtoPage_1 = KtoPage;
    KtoPage.prototype.showAgenda = function (date) {
        var parentElement = '.js-accordion_item';
        var activeClass = 'is-active';
        var container = '#js-accordion';
        var $this = $('#js-accordion_title2_' + date);
        setTimeout(function () {
            if ($this.parent(parentElement).hasClass(activeClass)) {
                $this.parents(container).find(parentElement).removeClass(activeClass);
                $this.parent(parentElement).removeClass(activeClass);
            }
            else {
                $this.parents(container).find(parentElement).removeClass(activeClass);
                $this.parent(parentElement).addClass(activeClass);
            }
        }, 300);
    };
    KtoPage.prototype.goToNav = function (action) {
        if (action == 'tabAgenda') {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__agenda_agenda__["a" /* AgendaPage */]);
        }
        if (action == 'tabAnkieta') {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__ankieta_ankieta__["a" /* AnkietaPage */]);
        }
        if (action == 'tabKto') {
            this.navCtrl.push(KtoPage_1);
        }
        this.viewCtrl.dismiss();
    };
    KtoPage.prototype.backToWelcome = function () {
        var root = this.app.getRootNav();
        root.popToRoot();
    };
    KtoPage.prototype.logout = function () {
        localStorage.setItem('userData', null);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* Login */]);
        this.viewCtrl.dismiss();
    };
    KtoPage = KtoPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-kto',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\kto\kto.html"*/'<ion-content no-bounce scrollbar-y="false">\n\n<div class="b-mainWrapper is-category">\n\n    <div class="b-main">\n\n        <div class="c-accordion" id="js-accordion">\n\n            <div *ngFor="let item of whoiswho; let first = first" class="c-accordion_item js-accordion_item">\n\n                <h2 class="c-accordion_title js-accordion_title" id="js-accordion_title2_{{ item[1][0].visitor_id }}"\n\n                    tappable (tap)="showAgenda(item[1][0].visitor_id)">{{ item[0] }}\n\n                    <i class="icon-arrow02"></i>\n\n                </h2>\n\n                <article class="c-accordion_content" *ngFor="let member of item[1];">\n\n                    <div class="c-accordion_bio" style="padding-right: 35px;">\n\n                        <span class="c-accordion_thumb">\n\n                            <img src="http://api-ios.test001.pl/data/avatar/{{ member.visitor_id }}.png"\n\n                                 alt="{{ member.visitor_firstname }} {{ member.visitor_lastname }}">\n\n                        </span>\n\n                        <p class="c-accordion_name">{{ member.visitor_firstname }} {{ member.visitor_lastname }}</p>\n\n                        <p class="c-accordion_description">{{ member.visitor_stanowisko }}\n\n                            <br>{{ member.visitor_komentarz }}<br>\n\n                        </p>\n\n                        <p class="c-accordion_email">\n\n                            <a href="mailto:{{ member.visitor_email }}">{{ member.visitor_email }}</a>\n\n                        </p>\n\n                        <p class="c-accordion_phone">\n\n                            {{ member.visitor_tel }}\n\n                        </p>\n\n                        <div class="c-accordion_contact" style="right: 10px;">\n\n                            <a href="mailto:{{ member.visitor_email }}">\n\n                                <i class="icon-edit01"></i>\n\n                            </a>\n\n                            <a href="tel:{{ member.visitor_tel }}">\n\n                                <i class="icon-phone01"></i>\n\n                            </a>\n\n                        </div>\n\n                    </div>\n\n                </article>\n\n            </div>\n\n        </div>\n\n    </div>\n\n</div>\n\n</ion-content>'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\kto\kto.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], KtoPage);
    return KtoPage;
    var KtoPage_1;
}());

//# sourceMappingURL=kto.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Login; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__welcome_welcome__ = __webpack_require__(110);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var Login = (function () {
    function Login(navCtrl, viewCtrl, authService, app, alertCtrl, events) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.authService = authService;
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.userData = { "visitor_number": "" };
        events.publish('hideHeader', { isHidden: true });
        if (localStorage.getItem('userData')) {
            var data = JSON.parse(localStorage.getItem('userData'));
            if (data != null && data.visitor_id != '') {
                this.viewCtrl.dismiss();
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__welcome_welcome__["a" /* WelcomePage */]);
            }
        }
    }
    Login.prototype.login = function () {
        var _this = this;
        console.log('login method');
        this.authService.postData('loginSite', this.userData, 'visitor/check').then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                localStorage.setItem('userData', JSON.stringify(_this.responseData));
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__welcome_welcome__["a" /* WelcomePage */]);
                _this.viewCtrl.dismiss();
            }
            else {
                //błąd logowania
                _this.presentAlert();
            }
        }, function (err) {
            console.log('Error' + err);
        });
    };
    Login.prototype.presentAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Numer niepoprawny',
            subTitle: 'Wprowadź poprawny numer zaczynający się od "M..."',
            buttons: ['Zamknij']
        });
        alert.present();
    };
    Login = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\login\login.html"*/'<ion-content id="login">\n\n    <div class="b-mainWrapper is-login">\n\n        <div class="a-logo">\n\n            <img src="common/images/logo/logo.png" alt="Mylan">\n\n        </div>\n\n        <div class="b-main">\n\n            <h1 class="a-typo is-primary" style="top: 30px">\n\n                Mylan<br>Navigator\n\n            </h1>\n\n            <div class="c-form">\n\n                <form action="" id="js-login">\n\n                    <div class="c-form_row">\n\n                        <p class="c-form_headline">Logowanie</p>\n\n                    </div>\n\n                    <div class="c-form_row">\n\n                        <ion-label class="c-form_label">Podaj swój mNumber</ion-label>\n\n                        <div class="c-form_input">\n\n                            <ion-input type="text" [(ngModel)]="userData.visitor_number" [ngModelOptions]="{standalone: true}"\n\n                                   data-rule-required="true"\n\n                                   data-msg-required="Wpisz swój numer" style=""></ion-input>\n\n                        </div>\n\n                    </div>\n\n                    <div class="c-form_row is-submit">\n\n                        <button tappable (tap)="login()" class="c-form_btn">Zaloguj</button>\n\n                    </div>\n\n                </form>\n\n            </div>\n\n        </div>\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], Login);
    return Login;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MmNagrodyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__mm__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__nagrody_info__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__basket__ = __webpack_require__(177);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MmNagrodyPage = (function () {
    function MmNagrodyPage(viewCtrl, navCtrl, authService, app, events, modalCtrl) {
        var _this = this;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.app = app;
        this.events = events;
        this.modalCtrl = modalCtrl;
        //m123451
        this.userData = { visitor_id: '', points: 0, visitor_number: '' };
        this.produkty = [];
        this.objectKeys = Object.keys;
        var data2 = JSON.parse(localStorage.getItem('userData'));
        console.log(data2);
        this.userData.visitor_number = data2.visitor_number;
        this.userData.points = data2.visitor_points;
        this.userData.visitor_id = data2.visitor_id;
        this.authService.postData('nagrodyList', this.userData, 'mm/product-list').then(function (result) {
            _this.responseData = result;
            if (_this.responseData.status == 'ok') {
                var json = JSON.parse(_this.responseData.productList);
                console.log(json);
                for (var key in json)
                    if (json.hasOwnProperty(key))
                        _this.produkty.push([key, json[key]]); // each item is an array in format [key, value]
                console.log(_this.produkty);
            }
        }, function (err) {
        });
    }
    MmNagrodyPage.prototype.presentPopover = function (product) {
        var popover = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__nagrody_info__["a" /* MmProductInfoPage */], { product: product, user: this.userData });
        popover.present();
    };
    MmNagrodyPage.prototype.backToMm = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__mm__["a" /* MmPage */]);
        this.viewCtrl.dismiss();
    };
    MmNagrodyPage.prototype.goToCard = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__basket__["a" /* MmBasketPage */]);
        this.viewCtrl.dismiss();
    };
    MmNagrodyPage.prototype.countBasketItems = function () {
        if (localStorage.getItem('basketData')) {
            var data = JSON.parse(localStorage.getItem('basketData'));
            if (data != null) {
                return data.itemSum;
            }
        }
        return 0;
    };
    MmNagrodyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mm-nagrody',template:/*ion-inline-start:"D:\Projekty\nawigator_app\mylan-app\src\pages\mm\tabs\nagrody.html"*/'<ion-content no-bounce scrollbar-y="false">\n\n    <div class="b-mainWrapper is-awards">\n\n        <div class="b-main">\n\n            <header class="b-header">\n\n                <a (click)="backToMm()" class="is-backBtn"><i class="icon-arrow02"></i> Miles & More</a>\n\n                <label for="menu"><i class="icon-menu01"></i></label>\n\n            </header>\n\n            <div class="b-awards">\n\n                <h1 class="b-headline">Nagrody</h1>\n\n                <div class="b-awards_grid">\n\n                    <div class="b-awards_gridRow" *ngFor="let item of produkty;">\n\n                        <div class="b-awards_gridItem">\n\n                            {{ item[0] }} {{i}}\n\n                        </div>\n\n                        <div class="b-awards_gridItem" *ngFor="let product of item[1]">\n\n                            <a (click)="presentPopover(product);">\n\n                                <span class="is-info">{{ product.product_title }} <span>{{ product.product_points }} pkt.</span></span>\n\n                                <img src="http://api-ios.test001.pl/data/product_img/{{ product.mm_product_id }}.jpg" alt="">\n\n                            </a>\n\n                        </div>\n\n                    </div>\n\n                </div>\n\n                <div class="b-awards_footer">\n\n                    <a (click)="goToCard()" class="a-btn is-primary">\n\n                        <span>{{ countBasketItems() }}</span>\n\n                        <i class="icon-cart01"></i> Moje zamówienie\n\n                    </a>\n\n                    <p>Do wykorzystania pozostało {{ userData.points }} pkt.</p>\n\n                </div>\n\n            </div>\n\n        </div>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"D:\Projekty\nawigator_app\mylan-app\src\pages\mm\tabs\nagrody.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], MmNagrodyPage);
    return MmNagrodyPage;
}());

//# sourceMappingURL=nagrody.js.map

/***/ })

},[225]);
//# sourceMappingURL=main.js.map