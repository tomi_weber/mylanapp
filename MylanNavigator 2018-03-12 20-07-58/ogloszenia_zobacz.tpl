<script type="text/javascript" src="includes/javascript/jwplayer.js" ></script>
<script type="text/javascript">jwplayer.key="BXqfGX7a5biB7DB8Iy+hZle+9RruXS2VwVI2tQOqRsKraVnjPGigQgKzZPs=";</script>
<!-- INCLUDE szablon_glowny_open.tpl -->
<div id="page-content">
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="/">Home</a></li>
			<li><a href="#">Ogłoszenia</a></li>
			<li class="active">{IMIE} {MIASTO}</li>
		</ol>
		<!--end breadcrumb-->
		<div class="row">
			<div class="col-md-3 col-sm-4">
				<div class="sidebar">
					<div class="box filter">
                        <!-- INCLUDE filters.tpl -->
                    </div>

					<p align="center">{REKLAMA_OPEN_LEWA}</p>
                    <p align="center">{REKLAMA_OPEN_LEWA2}</p>
				</div>
				<!--end sidebar-->
			</div>
			<!--end col-md-3-->
			<div class="col-md-9 col-sm-8">
				<div class="main-content">
					<div class="title">
						<div class="left">
							<h1>{IMIE}<span class="rating"><i class="fa fa-star"></i>9.9</span></h1>
							<h3><a href="/szukaj,{KATEGORIA_SLUG},kat-{KATEGORIA}.html?wojewodztwo=&miasto={MIASTO}">{MIASTO}</a></h3>
						</div>
						<div class="right">
							<a href="#map" class="icon scroll"><i class="fa fa-map-marker"></i>Zobacz na mapie</a>
							<!-- IF UZYTKOWNIK -->
							<a href="#tab-contact" data-id="{ID}" data-title="{IMIE} {MIASTO}" data-toggle="modal" data-tab="true" data-target="#contact-modal" class="btn btn-primary btn-rounded">{_lang_napisz} wiadomość</a>
							<!-- ELSE -->
							<a href="konto.html?error=Strona wymaga zalogowania"  class="btn btn-primary btn-rounded">{_lang_napisz} wiadomość</a>
							<!-- ENDIF -->
						</div>
					</div>
					<!--end title-->
					<div class="row">
						<div class="col-md-8">
							<section id="gallery">
								<div class="gallery-detail">
									<!-- IF VIP -->
									<div class="ribbon"><div class="offer-number">VIP</div><figure>Ogłoszenie</figure></div>
									<!-- ENDIF -->
									<div class="one-item-carousel">
										<!-- BEGIN zdjecia -->
										<div class="image">
											<img src="{zdjecia.ZDJECIE_DUZE}" onclick="jwplayer('my-video').stop();" alt="">
										</div>
										<!-- END zdjecia -->
										<!-- IF FILM -->
										<div class="image">
											<div>
												<div id='my-video'></div>
											</div>
											<script type='text/javascript'>
												jwplayer('my-video').setup({
													file: 'uploaded/videos/{FILM}',
													image: 'images/play.png',
													width: '480',
													height: '270'
												});
											</script>
										</div>
										<!-- ENDIF -->
									</div>
								</div>
							</section>
						</div>
						<div class="col-md-4">
							<div class="sidebar">
								<!-- IF KATEGORIA == 1 || KATEGORIA == 2 || KATEGORIA == 3 -->
								<h2>Dane podstawowe</h2>
								<aside class="box">
									<dl>
										<!-- IF WIEK -->
										<dt>{_lang_wiek}:</dt>
										<dd>{WIEK} {_lang_lat}</dd>
										<!-- ENDIF -->
										<!-- IF WZROST -->
										<dt>{_lang_wzrost}:</dt>
										<dd>{WZROST} {_lang_cm}</dd>
										<!-- ENDIF -->
										<!-- IF WAGA -->
										<dt>{_lang_waga}:</dt>
										<dd>{WAGA} {_lang_kg}</dd>
										<!-- ENDIF -->
										<!-- IF BIUST -->
										<dt>{_lang_biust}:</dt>
										<dd>{BIUST}</dd>
										<!-- ENDIF -->
									</dl>
								</aside>
								<!-- ENDIF -->
								<!-- IF USLUGI_DODATKOWE -->
								<section id="facilities">
									<h2>{_lang_w_trakcie_spotkania}</h2>
									<ul class="bullets half ">
										<!-- BEGIN uslugi_dodatkowe -->
										<!-- IF uslugi_dodatkowe.CHECKED -->
										<li> {uslugi_dodatkowe.NAZWA}</li>
										<!-- ENDIF -->
										<!-- END uslugi_dodatkowe -->
									</ul>
								</section>
								<!-- ENDIF -->
							</div>
						</div>
						<div class="row">
							<div class="col-md-8">
								<ul class="nav nav-tabs" role="tablist">
									<li role="presentation" class="active"><a href="#opis" aria-controls="opis" role="tab" data-toggle="tab">Opis</a></li>
									<li role="presentation"><a href="#mapa" aria-controls="mapa" role="tab" data-toggle="tab">Mapa</a></li>
									<!-- IF GODZINY_PRACY --><li role="presentation"><a href="#godziny" aria-controls="godziny" role="tab" data-toggle="tab">Godziny pracy</a></li><!-- ENDIF -->
									<li role="presentation"><a href="#komentarze" aria-controls="komentarze" role="tab" data-toggle="tab">Komentarze</a></li>
								</ul>

								<div class="tab-content" style="margin: 15px 0;">
									<div role="tabpanel" class="tab-pane active" id="opis">
										<h2>{_lang_opis}</h2>

										<section id="description">
											<p>{TRESC_OGLOSZENIA}</p>
										</section>
										<section>
											<div class="row">
												<!-- IF CENA_ZA_1H -->
												<div class="col-md-4 col-sm-4">
													<div class="feature">
														<aside class="circle">
															<i class="icon_box-checked"></i>
														</aside>
														<figure style="padding-top: 4px;">
															<h5 style="line-height: 1.3"><strong>{_lang_cena_za_godzine}:</strong><br />{CENA_ZA_1H} {_lang_zl}</h5>
														</figure>
													</div>
													<!--end feature-->
												</div>
												<!-- ENDIF -->
												<!-- IF CENA_ZA_NOC -->
												<div class="col-md-4 col-sm-4">
													<div class="feature">
														<aside class="circle">
															<i class="icon_box-checked"></i>
														</aside>
														<figure style="padding-top: 4px;">
															<h5 style="line-height: 1.3"><strong>{_lang_cena_za_noc}:</strong><br />{CENA_ZA_1H} {_lang_zl}</h5>
														</figure>
													</div>
													<!--end feature-->
												</div>
												<!-- ENDIF -->
												<!-- IF CENA_ZA_30M -->
												<div class="col-md-4 col-sm-4">
													<div class="feature">
														<aside class="circle">
															<i class="icon_box-checked"></i>
														</aside>
														<figure style="padding-top: 4px;">
															<h5 style="line-height: 1.3"><strong>{_lang_cena_za_30min}:</strong><br />{CENA_ZA_30M} {_lang_zl}</h5>
														</figure>
													</div>
													<!--end feature-->
												</div>
												<!-- ENDIF -->
											</div>
											<!--end row-->
										</section>
									</div>

									<div role="tabpanel" class="tab-pane" id="mapa">
										<section id="map">
											<h2>Mapa</h2>
											<div id="map-item" class="map height-300 box"></div>
											<!--end map-->
										</section>
									</div>

									<!-- IF GODZINY_PRACY -->
									<div role="tabpanel" class="tab-pane" id="godziny">
										<section id="additional-information">
											<h2>{_lang_godziny_pracy}</h2>
											<dl class="info">
												<!-- BEGIN godziny_pracy -->
												<dt>{godziny_pracy.DZIEN}</dt>
												<dd>{godziny_pracy.GODZINY}&nbsp;</dd>
												<!-- END godziny_pracy -->
											</dl>
										</section>
									</div>
									<!-- ENDIF -->

									<div role="tabpanel" class="tab-pane" id="komentarze">
										<section id="reviews">
											<div class="title">
												<h2 class="pull-left">{_lang_komentarze}</h2>
												<a href="#write-a-review" class="btn btn-primary btn-rounded pull-right scroll">{_lang_dodaj_komentarz}</a>
											</div>
											<!-- IF OCENA_SREDNIA -->
											<h3>{_lang_srednia_ocena}</h3>
											<ul class="rating-score">
												<li class="overall"><i class="fa fa-star"></i>{OCENA_SREDNIA}</li>
											</ul>
											<!-- ENDIF -->
											<!-- IF KATEGORIA != 3 -->
											<div class="reviews">
												<!-- BEGIN komentarze -->
												<div class="review">
													<div class="row">
														<div class="col-md-3">
															<aside class="name">{komentarze.UZYTKOWNIK}</aside>
															<aside class="date">{komentarze.DATA_DODANIA}</aside>
														</div>
														<!--end col-md-3-->
														<div class="col-md-9">
															<div class="comment">
																<div class="comment-title">
																	<figure class="rating">{komentarze.OCENA}/5</figure>
																	<h4>{komentarze.KOMENTARZ_TYTUL}</h4>
																</div>
																<!--end title-->
																<p>{komentarze.KOMENTARZ}</p>
															</div>
															<!--end comment-->
														</div>
														<!--end col-md-9-->
													</div>
													<!--end row-->
												</div>
												<!-- END komentarze -->
											</div>
											<!--end reviews-->
											<!-- ENDIF -->
										</section>
										<!-- IF KATEGORIA != 3 -->
										<section id="write-a-review">
											<h2>{_lang_dodaj_komentarz}</h2>
											<form  class="labels-uppercase clearfix" id="form_reply_1" method="post" action="">
												<div class="review write" id="review-write">
													<div class="comment">
														<div class="row">
															<div class="col-md-8">
																<div class="comment-title">
																	<h4>Zostaw swoją ocenę</h4>
																</div>
																<!--end title-->
																<div class="form-group">
																	<label for="form_reply_1-name">Tytuł twojej oceny<em>*</em></label>
																	<input type="text" class="form-control" id="form_reply_1-name" name="komentarz_title" required="true">
																</div>
																<div class="form-group">
																	<label for="form_reply_1-message">Twój komentarz<em>*</em></label>
																	<textarea class="form-control" id="form_reply_1-message" rows="8" name="komentarz" required="true">{KOMENTARZ_REQUEST}</textarea>
																</div>
																<!--end form-group-->
																<div class="form-group pull-right">
																	<button type="submit" class="btn btn-primary btn-rounded">{_lang_wyslij}</button>
																</div>
																<!--end form-group-->
															</div>
															<!--end col-md-8-->
															<div class="col-md-4">
																<div class="comment-title">
																	<h4>{_lang_ocena}</h4>
																</div>
																<!--end title-->
																<dl class="visitor-rating">
																	<dd class="star-rating active" data-name="ocena"></dd>
																</dl>
															</div>
															<!--end col-md-4-->
														</div>
														<!--end row-->
													</div>
													<!--end comment-->
												</div>
												<!--end review-->
											</form>
											<!--end form-->
										</section>
										<!-- ENDIF -->
									</div>
								</div>
							</div>

							<!--end col-md-8-->
							<div class="col-md-4">
								<div class="sidebar">
									<aside>
										<h2>Podobne ogłoszenia</h2>
										<ul class="half">
											<!-- BEGIN inne -->
											<li style="padding: 10px;">
												<a href="/ogl,{inne.SLUG},id-{inne.ID}.html"><h3>{inne.IMIE}</h3></a>
												<p>{inne.MIASTO}</p>
												<img style="{inne.WYMIAR_100};" alt="{inne.IMIE}" src="{inne.ZDJECIE}" />
											</li>
											<!-- END inne -->
											</tbody>
										</ul>
									</aside>
								</div>
								<!--end sidebar-->
							</div>
							<!--end col-md-4-->
						</div>
						<!--end row-->
					</div>
				</div>
				<!--end main-content-->
			</div>
			<!--end col-md-9-->
		</div>
		<!--end row-->
	</div>
	<!--end container-->
</div>
<!--end page-content-->
<!-- INCLUDE szablon_glowny_close.tpl -->