<?php
/**
 * Created by PhpStorm.
 * User: Tomek
 * Date: 2017-12-06
 * Time: 16:46
 */
namespace Controller;

use News\Model\News;
use News\Model\NewsRecipient;
use Visitor\Model\Visitor;

class NewsController
{
    protected $view;

    // constructor receives container instance
    public function __construct($container, $router)
    {
        $this->view = $container;
        $this->router = $router;
    }

    public function index($request, $response, $args)
    {

        $modelResult = new News();
        $resultData = $modelResult->getAll(array(
			'news_score' => 0
		), array(
            'sort'  => 'news_id',
            'order' => 'desc'
        ), array(
            'limit' => $args['last'],
            'start' => 0
        ));

        return $this->view->render($response, 'news.phtml', [
            'result' => $resultData
        ]);
    }
	
	public function indexScore($request, $response, $args)
    {

        $modelResult = new News();
        $resultData = $modelResult->getAll(array(
			'news_score' => 1
		), array(
            'sort'  => 'news_id',
            'order' => 'desc'
        ), array(
            'limit' => $args['last'],
            'start' => 0
        ));

        return $this->view->render($response, 'admin/score.phtml', [
            'result' => $resultData
        ]);
    }

    public function edit($request, $response, $args)
    {
        $modelNews = new News();
        $modelNews->load((int)$args['id']);

        if($request->isPost()) {
            $arrayData = $request->getParsedBody();

            if(array_key_exists('id', $args) && (int)$args['id'] > 0) {
                $id = $args['id'];
                $modelNews->update($arrayData, $args['id']);
            } else {
                $arrayData['news_date_add'] = date("Y-m-d");
                $id = $modelNews->insert($arrayData);
            }

            $modelNewsRecipient = new NewsRecipient();
            $modelNewsRecipient->deleteWhere('news_id = ' . $id);

            if($arrayData['recipient_all'] != 1) {

                if (!is_array($arrayData['recipient_line'])) {
                    $arrayData['recipient_line'] = array($arrayData['recipient_line']);
                }

                foreach ($arrayData['recipient_line'] as $line) {
                    if($line != '') {
                        $modelNewsRecipient->insert(array(
                            'news_id'         => $id,
                            'recipient_key'   => 'line',
                            'recipient_value' => $line,
                        ));
                    }
                }

                if (!is_array($arrayData['recipient_function'])) {
                    $arrayData['recipient_function'] = array($arrayData['recipient_function']);
                }

                foreach ($arrayData['recipient_function'] as $line) {
                    if($line != '') {
                        $modelNewsRecipient->insert(array(
                            'news_id'         => $id,
                            'recipient_key'   => 'function',
                            'recipient_value' => $line,
                        ));
                    }
                }
            } else {
                $modelNewsRecipient->insert(array(
                    'news_id'         => $id,
                    'recipient_all'   => 1,
                ));
            }

            if($_FILES != null && is_array($_FILES)) {
                if (0 < $_FILES['file']['error']) {

                } else {
                    $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                    $filename = dirname(__FILE__) . '/../../public/data/news_images/' . $id . '.' . $ext;
                    move_uploaded_file(
                        $_FILES['file']['tmp_name'],
                        $filename
                    );
					
					$modelNews->update(array(
						'news_photo' =>  $id . '.' . $ext
					), $id);
                }
            }

            return $response->withJson(array(
                'status' => 'success'
            ), 200);
        }

        $modelVisitor = new Visitor();
        $resultData = $modelVisitor->getAll(array(), array(
            'sort'  => 'visitor_line',
            'order' => 'asc'
        ));

        foreach($resultData as $line) {
            if($line['visitor_line'] != '') {
                $tmp = explode(',', $line['visitor_line']);
                foreach($tmp as $tmp2) {
                    $lines[$tmp2] = $tmp2;
                }
            }
        }

        return $this->view->render($response, 'news-edit.phtml', [
            'result' => $modelNews->getData(),
            'lines' => $lines,
            'id' => $args['id']
        ]);
    }
	
	public function editScore($request, $response, $args)
    {
        $modelNews = new News();
        $modelNews->load((int)$args['id']);

        if($request->isPost()) {
            $arrayData = $request->getParsedBody();

            if(array_key_exists('id', $args) && (int)$args['id'] > 0) {
                $id = $args['id'];
                $modelNews->update($arrayData, $args['id']);
            } else {
                $arrayData['news_date_add'] = date("Y-m-d");
                $id = $modelNews->insert($arrayData);
            }

            $modelNewsRecipient = new NewsRecipient();
            $modelNewsRecipient->deleteWhere('news_id = ' . $id);

            if($arrayData['recipient_all'] != 1) {

                if (!is_array($arrayData['recipient_line'])) {
                    $arrayData['recipient_line'] = array($arrayData['recipient_line']);
                }

                foreach ($arrayData['recipient_line'] as $line) {
                    if($line != '') {
                        $modelNewsRecipient->insert(array(
                            'news_id'         => $id,
                            'recipient_key'   => 'line',
                            'recipient_value' => $line,
                        ));
                    }
                }

                if (!is_array($arrayData['recipient_function'])) {
                    $arrayData['recipient_function'] = array($arrayData['recipient_function']);
                }

                foreach ($arrayData['recipient_function'] as $line) {
                    if($line != '') {
                        $modelNewsRecipient->insert(array(
                            'news_id'         => $id,
                            'recipient_key'   => 'function',
                            'recipient_value' => $line,
                        ));
                    }
                }
            } else {
                $modelNewsRecipient->insert(array(
                    'news_id'         => $id,
                    'recipient_all'   => 1,
                ));
            }

           

            return $response->withJson(array(
                'status' => 'success'
            ), 200);
        }

        $modelVisitor = new Visitor();
        $resultData = $modelVisitor->getAll(array(), array(
            'sort'  => 'visitor_line',
            'order' => 'asc'
        ));

        foreach($resultData as $line) {
            if($line['visitor_line'] != '') {
                $tmp = explode(',', $line['visitor_line']);
                foreach($tmp as $tmp2) {
                    $lines[$tmp2] = $tmp2;
                }
            }
        }

        return $this->view->render($response, 'admin/score-edit.phtml', [
            'result' => $modelNews->getData(),
            'lines' => $lines,
            'id' => $args['id']
        ]);
    }

    public function delete($request, $response, $args) {

        if($request->isPost()) {
            $arrayData = $request->getParsedBody();

            $modelNews = new News();
            $news = $modelNews->getOne(array(
                'news_id' => $arrayData['id']
            ));

            $modelNews->delete($news['news_id']);

            return $response->withJson(array(
                'status' => 'success'
            ), 200);
        }

        $modelNews = new News();
        $modelNews->load((int)$args['id']);

        return $this->view->render($response, 'news-delete.phtml', [
            'result' => $modelNews->getData(),
            'id' => $args['id']
        ]);
    }
	
	public function deleteScore($request, $response, $args) {
		
		if($request->isPost()) {
            $arrayData = $request->getParsedBody();

            $modelNews = new News();
            $news = $modelNews->getOne(array(
                'news_id' => $arrayData['id']
            ));

            $modelNews->delete($news['news_id']);

            return $response->withJson(array(
                'status' => 'success'
            ), 200);
        }

        $modelNews = new News();
        $modelNews->load((int)$args['id']);

        return $this->view->render($response, 'admin/score-delete.phtml', [
            'result' => $modelNews->getData(),
            'id' => $args['id']
        ]);
	}

    public function sendMessage($request, $response, $args) {

        $modelNews = new News();
        $modelNews->load((int)$args['id']);

        $newsRow = $modelNews->getData();
        if(is_array($newsRow) && count($newsRow) > 0) {

            $contentTitle = array(
                "en" => $newsRow['news_title'],
                "pl" => $newsRow['news_title'],
            );

            $contentTxt = array(
                "en" => substr($newsRow['news_desc'], 0, 250) . '...',
                "pl" => substr($newsRow['news_desc'], 0, 250) . '...',
            );

            $fields = array(
                'app_id'   => "73b5d8a1-0f4f-481a-8d2b-cdb8956f9569",
                'data'     => array("news_id" => (int)$args['id']),
                'contents' => $contentTxt,
                'headings' => $contentTitle
            );

            if($newsRow['recipient_all'] == 1) {
                $segments = array();
                $segments['included_segments'] = array('All');

                $fields = array_merge($fields, $segments);
            } else {
                $filters = array();
                if(!empty($newsRow['recipient_line']) && count($newsRow['recipient_line']) > 0) {
                    foreach ($newsRow['recipient_line'] as $line) {
                        if ($line != '') {
							for($i=0;$i<8;$i++) {
								$filters[] = array(
									'field'    => 'tag',
									'key'      => 'line' . $i,
									'relation' => '=',
									'value'    => $line,
								);

								$filters[] = array(
									"operator" => "OR"
								);
							}
                        }
                    }
                }

                if(!empty($newsRow['recipient_function']) && count($newsRow['recipient_function']) > 0) {
                    foreach ($newsRow['recipient_function'] as $function) {
                        if ($function != '') {
                            $filters[] = array(
                                'field'    => 'tag',
                                'key'      => 'function',
                                'relation' => '=',
                                'value'    => $function,
                            );

                            $filters[] = array(
                                "operator" => "OR"
                            );
                        }
                    }
                }

                if(count($filters) > 0 && !empty($filters)) {
                    array_pop($filters);
                    $filters = array('filters' => $filters);

                    $fields = array_merge($fields, $filters);
                }
            }

            $fields = json_encode($fields);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                'Authorization: Basic NTA5MWY5OWQtM2IwYi00NDljLTlhOWMtNWQxZDY0MDI1MmRi'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            $res = curl_exec($ch);
            curl_close($ch);

            $return = json_encode($res);
            if($return['recipients']) {
                $modelNews->update(
                    array(
                        'news_send' => 1,
                        'news_send_date' => date('Y-m-d H:i:s'),
                    )
                );

                return $response->withJson(array(
                    'status' => 'success',
                    'info'     => $return['recipients']
                ), 200);


            } else {
                return $response->withJson(array(
                    'status' => 'error',
                ), 200);
            }
        }

        return $response;
    }

    public function getPersonalisedList($request, $response, $args) {

        if($request->isPost()) {

            $json = $request->getBody();
            $arrayData = json_decode($json, true);
            if ($arrayData['visitor_id'] != '' && $arrayData['visitor_id'] != 'undefined') {

                $modelVisitor = new Visitor();
                $modelVisitor->load((int)$arrayData['visitor_id']);

                $visitorData = $modelVisitor->getData();
                $returnData = array();
				
				$visitorLine = array();
				$arr = explode(',', $visitorData['visitor_line']);
				foreach ($arr as $value) {
					$visitorLine[] = trim($value);
				}

                $db = \Db\Db::getInstance();
                $sql = 'SELECT news_id FROM mylan_news_recipient WHERE recipient_all = 1';
                if($visitorData['visitor_line'] != '') {
                    $sql .= ' OR (recipient_key = "line" AND recipient_value IN ("' . implode('","', $visitorLine) . '"))';
                }

                if($visitorData['visitor_function'] != '') {
                    $sql .= ' OR (recipient_key = "function" AND recipient_value = "' . $visitorData['visitor_function'] . '")';
                }

                $stmt = $db->query($sql);
                $result = $stmt->fetchAll();
                if(!empty($result) && count($result) > 0) {
                    $newsIds = array_column($result, 'news_id');

                    $modelResult = new News();
                    $returnData = $modelResult->getAll(array(
                        'news_id' => $newsIds,
						'news_score' => 0
                    ), array(
                        'sort'  => 'news_id',
                        'order' => 'desc'
                    ), array(
                        'limit' => $args['last'],
                        'start' => 0
                    ));
					
					$newsData = array();
					foreach($returnData as $row) {
						$row['news_desc_full'] = '';
						$row['news_desc'] = html_entity_decode($row['news_desc']);
						if(strlen($row['news_desc']) > 120) {
							$row['news_desc_full'] = $row['news_desc'];
							$row['news_desc'] = substr($row['news_desc'], 0 ,120) . '...';
						}
						
						$newsData[] = $row;
					}
					
                }


                return $response
                    ->withHeader('Access-Control-Allow-Origin', '*')
                    ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                    ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
                    ->withJson(array(
                        'status' => 'ok',
                        'info' => 'ok',
                        'userData' => json_encode($newsData),
                    ), 200);
            } else {
                return $response
                    ->withHeader('Access-Control-Allow-Origin', '*')
                    ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                    ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
                    ->withJson(array(
                        'Log'  => 'error',
                        'info' => 'error'
                    ), 200);
            }
        }
    }
	
	public function getPersonalisedListScore($request, $response, $args) {
		
		if($request->isPost()) {

            $json = $request->getBody();
            $arrayData = json_decode($json, true);
            if ($arrayData['visitor_id'] != '' && $arrayData['visitor_id'] != 'undefined') {

                $modelVisitor = new Visitor();
                $modelVisitor->load((int)$arrayData['visitor_id']);

                $visitorData = $modelVisitor->getData();
                $returnData = array();
				
				$visitorLine = array();
				$arr = explode(',', $visitorData['visitor_line']);
				foreach ($arr as $value) {
					$visitorLine[] = trim($value);
				}

                $db = \Db\Db::getInstance();
                $sql = 'SELECT news_id FROM mylan_news_recipient WHERE recipient_all = 1';
                if($visitorData['visitor_line'] != '') {
                    $sql .= ' OR (recipient_key = "line" AND recipient_value IN ("' . implode('","', $visitorLine) . '"))';
                }

                if($visitorData['visitor_function'] != '') {
                    $sql .= ' OR (recipient_key = "function" AND recipient_value = "' . $visitorData['visitor_function'] . '")';
                }

                $stmt = $db->query($sql);
                $result = $stmt->fetchAll();
                if(!empty($result) && count($result) > 0) {
                    $newsIds = array_column($result, 'news_id');

                    $modelResult = new News();
                    $returnData = $modelResult->getAll(array(
                        'news_id' => $newsIds,
						'news_score' => 1,
						'news_send' => 1
                    ), array(
                        'sort'  => 'news_id',
                        'order' => 'desc'
                    ), array(
                        'limit' => $args['last'],
                        'start' => 0
                    ));
					
					$newsData = array();
					foreach($returnData as $row) {	
						$row['news_desc'] = html_entity_decode($row['news_desc']);					
						$newsData[] = $row;
					}
					
                }


                return $response
                    ->withHeader('Access-Control-Allow-Origin', '*')
                    ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                    ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
                    ->withJson(array(
                        'status' => 'ok',
                        'info' => 'ok',
                        'userData' => json_encode($newsData),
                    ), 200);
            } else {
                return $response
                    ->withHeader('Access-Control-Allow-Origin', '*')
                    ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                    ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
                    ->withJson(array(
                        'Log'  => 'error',
                        'info' => 'error'
                    ), 200);
            }
        }
	}

}